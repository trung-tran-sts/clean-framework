﻿using Microsoft.Extensions.DependencyInjection;

namespace CleanFramework.BuildingBlocks.PubSub.ApacheKafka
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddKafkaIntegration(this IServiceCollection services)
        {
            return services.AddSingleton<IEventPublisher, ApacheKafkaEventPublisher>()
                .AddSingleton<IEventSubscriber, ApacheKafkaEventSubscriber>();
        }
    }
}
