﻿using Confluent.Kafka;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace CleanFramework.BuildingBlocks.PubSub.ApacheKafka
{
    public class ApacheKafkaEventPublisher : IEventPublisher, IDisposable
    {
        private readonly ProducerConfig _producerConfig;
        private readonly ILogger<ApacheKafkaEventPublisher> _logger;
        private readonly ConcurrentDictionary<string, IClient> _producerPerTopics;
        private bool _disposedValue;

        public ApacheKafkaEventPublisher(IOptions<ProducerConfig> settings,
            ILogger<ApacheKafkaEventPublisher> logger)
        {
            _producerConfig = settings.Value;
            _logger = logger;
            _producerPerTopics = new ConcurrentDictionary<string, IClient>();
        }

        public async Task PublishAsync<T>(string topic, string key, T @event)
        {
            var producer = _producerPerTopics.GetOrAdd(topic, (_) => CreateProducer<T>()) as IProducer<string, T>;

            var result = await producer.ProduceAsync(topic, new Message<string, T>()
            {
                Key = key,
                Value = @event
            });

            _logger.LogInformation("------ Produce message key '{Key}' status '{Status}'", key, result.Status);
        }

        private IProducer<string, T> CreateProducer<T>()
        {
            var producer = new ProducerBuilder<string, T>(_producerConfig)
                .SetValueSerializer(new SimpleJsonSerdes<T>())
                .Build();

            return producer;
        }

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposedValue = true;

                foreach (var producer in _producerPerTopics.Values)
                    producer.Dispose();
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ApacheKafkaConfiguration()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
