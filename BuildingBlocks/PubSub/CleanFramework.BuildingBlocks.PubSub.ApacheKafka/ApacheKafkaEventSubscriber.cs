﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace CleanFramework.BuildingBlocks.PubSub.ApacheKafka
{
    public class ApacheKafkaEventSubscriber : IEventSubscriber
    {
        private readonly IServiceProvider _serviceProvider;
        public ApacheKafkaEventSubscriber(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ISubscription Subscribe<SubscriptionType>(string topic) where SubscriptionType : class
        {
            if (!typeof(IApacheKafkaSubscription).IsAssignableFrom(typeof(SubscriptionType)))
            {
                throw new ArgumentException(nameof(SubscriptionType));
            }

            var subscription = _serviceProvider.GetRequiredService<SubscriptionType>()
                as IApacheKafkaSubscription;

            subscription.StartConsume(topic);

            return subscription;
        }
    }
}
