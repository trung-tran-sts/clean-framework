﻿using Confluent.Kafka;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.BuildingBlocks.PubSub.ApacheKafka
{
    public interface IApacheKafkaSubscription : ISubscription
    {
        void StartConsume(string topic);
    }

    public abstract class ApacheKafkaSubscription<T> : IApacheKafkaSubscription
    {
        private readonly ConsumerConfig _consumerConfig;
        private Thread _currentWorkerThread;
        private CancellationTokenSource _cancellationTokenSource;

        public ApacheKafkaSubscription(IOptions<ConsumerConfig> consumerConfig)
        {
            _consumerConfig = consumerConfig.Value;
        }

        public bool IsListening { get; private set; }

        private string _consumingTopic;
        public string ConsumingTopic => _consumingTopic;

        public virtual void StartConsume(string topic)
        {
            _consumingTopic = topic;
            _cancellationTokenSource = new CancellationTokenSource();
            _currentWorkerThread = new Thread(new ParameterizedThreadStart(
                async (tokenSource) => await DoWorkAsync(_cancellationTokenSource.Token)));
            _currentWorkerThread.IsBackground = true;
            _currentWorkerThread.Start();
            IsListening = true;
        }

        public virtual void Unsubscribe()
        {
            if (!IsListening)
            {
                throw new InvalidOperationException("Not listening");
            }

            _cancellationTokenSource.Cancel();
            _currentWorkerThread = null;
            _cancellationTokenSource = null;
            IsListening = false;
        }

        protected abstract Task DoWorkAsync(CancellationToken cancellationToken);

        protected virtual IConsumer<string, T> CreateConsumer()
        {
            var consumer = new ConsumerBuilder<string, T>(_consumerConfig)
                .SetValueDeserializer(new SimpleJsonSerdes<T>()).Build();

            return consumer;
        }
    }
}
