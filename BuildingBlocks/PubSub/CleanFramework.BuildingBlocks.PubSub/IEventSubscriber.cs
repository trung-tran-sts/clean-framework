﻿namespace CleanFramework.BuildingBlocks.PubSub
{
    public interface IEventSubscriber
    {
        ISubscription Subscribe<T>(string topic) where T : class;
    }

    public class NullEventSubscriber : IEventSubscriber
    {
        public ISubscription Subscribe<T>(string topic) where T : class
        {
            return new NullSubscription();
        }
    }
}
