﻿namespace CleanFramework.BuildingBlocks.PubSub
{
    public interface ISubscription
    {
        string ConsumingTopic { get; }
        void Unsubscribe();
    }

    public class NullSubscription : ISubscription
    {
        public string ConsumingTopic => string.Empty;

        public void Unsubscribe()
        {
        }
    }
}
