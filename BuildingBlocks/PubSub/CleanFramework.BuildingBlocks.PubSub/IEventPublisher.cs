﻿using System.Threading.Tasks;

namespace CleanFramework.BuildingBlocks.PubSub
{
    public interface IEventPublisher
    {
        Task PublishAsync<T>(string topic, string key, T @event);
    }

    public class NullEventPublisher : IEventPublisher
    {
        public Task PublishAsync<T>(string topic, string key, T @event)
        {
            return Task.CompletedTask;
        }
    }
}
