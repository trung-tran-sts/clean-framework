﻿using Microsoft.Extensions.DependencyInjection;

namespace CleanFramework.BuildingBlocks.PubSub
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNullPubSub(this IServiceCollection services)
        {
            return services.AddSingleton<IEventPublisher, NullEventPublisher>()
                .AddSingleton<IEventSubscriber, NullEventSubscriber>();
        }
    }
}
