﻿using CleanFramework.ClientSDK;
using CleanFramework.ClientSDK.Order;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Polly;
using Polly.Contrib.WaitAndRetry;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CleanFramework.ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var services = new ServiceCollection()
                .Configure<OrderClientSDKOptions>(opt =>
                {
                    opt.ClientId = "137A8C76BAFA5";
                    opt.ClientSecret = "roemBQDSJfTC8PfzgP23vGylWraDoDSb";
                    opt.OrderServerUrl = "https://localhost:5001";
                });

            var jitterDelay = Backoff.DecorrelatedJitterBackoffV2(medianFirstRetryDelay: TimeSpan.FromSeconds(2), retryCount: 3);

            var retry = Policy<HttpResponseMessage>
                .HandleResult(resp => (int)resp.StatusCode >= (int)HttpStatusCode.InternalServerError)
                .Or<HttpRequestException>()
                .WaitAndRetryAsync(jitterDelay,
                    onRetry: (result, sleepDur, attempt, context) => Console.WriteLine($"Retry {attempt}"));

            services.AddOrderClientSDK()
                .AddPolicyHandler(retry);
            //.AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(jitterDelay));

            var rootProvider = services.BuildServiceProvider();

            await CallServiceAsync(rootProvider);
            await CallServiceAsync(rootProvider);
            await CallServiceAsync(rootProvider);
        }

        static async Task CallServiceAsync(IServiceProvider rootProvider)
        {
            using var scope = rootProvider.CreateScope();
            var scopeProvider = scope.ServiceProvider;

            var orderClient = scopeProvider.GetRequiredService<IOrderClient>();
            var cardTypes = await orderClient.GetCardTypesAsync();
            var jsonStr = JsonConvert.SerializeObject(cardTypes, Formatting.Indented);

            Console.WriteLine(jsonStr);
        }
    }
}
