﻿using CleanFramework.ClientSDK.Constants;
using CleanFramework.ClientSDK.Order;
using CleanFramework.ClientSDK.Order.Handlers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace CleanFramework.ClientSDK
{
    public static class ServiceCollectionExtensions
    {
        public static IHttpClientBuilder AddOrderClientSDK(this IServiceCollection services)
        {
            services.AddSingleton<IOrderClient, OrderClient>()
                .AddSingleton<ClientCredentialsHttpHandler>();

            return services.AddHttpClient(ClientNames.Order, (provider, httpClient) =>
            {
                var clientOptionsSnapshot = provider.GetRequiredService<IOptions<OrderClientSDKOptions>>();
                var clientOptions = clientOptionsSnapshot.Value;
                httpClient.BaseAddress = new Uri(clientOptions.OrderServerUrl);
            }).AddHttpMessageHandler<ClientCredentialsHttpHandler>();
        }
    }
}
