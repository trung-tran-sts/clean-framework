﻿using CleanFramework.ClientSDK.Constants;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.ClientSDK.Order.Handlers
{
    public class ClientCredentialsHttpHandler : DelegatingHandler
    {
        private readonly IOptions<OrderClientSDKOptions> _options;

        public ClientCredentialsHttpHandler(IOptions<OrderClientSDKOptions> options)
        {
            _options = options;
        }

        public ClientCredentialsHttpHandler(HttpMessageHandler innerHandler,
            IOptions<OrderClientSDKOptions> options) : base(innerHandler)
        {
            _options = options;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var clientOptions = _options.Value;

            var authHeader = Convert.ToBase64String(
                Encoding.UTF8.GetBytes(
                    $"{clientOptions.ClientId}:{clientOptions.ClientSecret}"));

            request.Headers.Authorization = new AuthenticationHeaderValue(
                AuthConstants.ClientScheme, authHeader);

            return base.SendAsync(request, cancellationToken);
        }
    }
}
