﻿namespace CleanFramework.ClientSDK.Order
{
    public class OrderClientSDKOptions
    {
        public string OrderServerUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
