﻿namespace CleanFramework.ClientSDK.Order.Models
{
    public class CardTypeListItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
