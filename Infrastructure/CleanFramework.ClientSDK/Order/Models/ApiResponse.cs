﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace CleanFramework.ClientSDK.Order.Models
{
    public class ApiResponse<T>
    {
        public int Code { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }

        [JsonExtensionData]
        public IDictionary<string, JToken> Extensions { get; set; }
    }
}
