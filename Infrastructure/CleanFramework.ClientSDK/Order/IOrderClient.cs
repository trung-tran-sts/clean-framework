﻿using CleanFramework.ClientSDK.Order.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanFramework.ClientSDK.Order
{
    public interface IOrderClient
    {
        Task<IEnumerable<CardTypeListItem>> GetCardTypesAsync();
    }
}
