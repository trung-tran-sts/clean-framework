﻿using CleanFramework.ClientSDK.Constants;
using CleanFramework.ClientSDK.Order.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CleanFramework.ClientSDK.Order
{
    public class OrderClient : IOrderClient
    {
        private readonly HttpClient _client;

        public OrderClient(IHttpClientFactory httpClientFactory)
        {
            _client = httpClientFactory.CreateClient(ClientNames.Order);
        }

        public async Task<IEnumerable<CardTypeListItem>> GetCardTypesAsync()
        {
            var apiEndpoint = new PathString(ApiEndpoints.External.CardType.GetCardTypes);

            var response = await _client.GetAsync(apiEndpoint);

            response = response.EnsureSuccessStatusCode();

            var apiResponseStr = await response.Content.ReadAsStringAsync();
            var apiResponse = JsonConvert.DeserializeObject<ApiResponse<IEnumerable<CardTypeListItem>>>(apiResponseStr);

            return apiResponse?.Data;
        }
    }
}