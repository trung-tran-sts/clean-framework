﻿namespace CleanFramework.ClientSDK.Order
{
    public static class ApiEndpoints
    {
        public static class External
        {
            public static class CardType
            {
                public const string GetCardTypes = "/api/external/card-types";
            }
        }
    }
}
