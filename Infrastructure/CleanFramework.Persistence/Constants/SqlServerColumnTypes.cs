﻿using System.Collections.Generic;

namespace CleanFramework.Persistence.Constants
{
    public static class SqlServerColumnTypes
    {
        public const string ntext = nameof(ntext);
        public const string text = nameof(text);

        public static readonly IEnumerable<string> TextColumnTypes = new[]
        {
            text,
            ntext
        };
    }
}
