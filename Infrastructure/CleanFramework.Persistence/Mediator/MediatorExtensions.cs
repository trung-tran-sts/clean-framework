﻿using CleanFramework.Domain.Base;
using MediatR;
using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Persistence.Mediator
{
    static class MediatorExtension
    {
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, OrderingContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<DomainEntity>()
                .Where(x => x.Entity.GetDomainEvents()?.Any() == true);

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.GetDomainEvents())
                .ToList();

            domainEntities.ToList().ForEach(entity => entity.Entity.ClearDomainEvents());

            foreach (var domainEvent in domainEvents)
                await mediator.Publish(domainEvent);
        }
    }
}
