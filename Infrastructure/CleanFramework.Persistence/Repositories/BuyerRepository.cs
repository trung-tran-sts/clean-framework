﻿using CleanFramework.Common.DependencyInjection;
using CleanFramework.Domain.Aggregates.BuyerAggregate;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Persistence.Repositories
{
    public class BuyerRepository :
        Repository<Buyer, int>,
        IBuyerRepository,
        IScopedService
    {
        public BuyerRepository(OrderingContext context) : base(context)
        {
        }

        public override async Task<Buyer> FindByIdAsync(int id)
        {
            var buyer = await context.Buyer
                .Include(b => b.PaymentMethods)
                .Where(b => b.Id == id)
                .SingleOrDefaultAsync();

            if (buyer == null)
            {
                buyer = context.Buyer
                    .Local
                    .SingleOrDefault(o => o.Id == id);
            }

            return buyer;
        }
    }
}
