﻿using CleanFramework.Domain.Base;
using CleanFramework.Persistence.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Persistence.Repositories
{
    public abstract class Repository<T, TKey> :
        IRepository<T, TKey>
        where T : class, IAggregateRoot
    {
        protected readonly OrderingContext context;

        public IUnitOfWork UnitOfWork => context;

        public IQueryable<T> NoTrackedQuery => context.Set<T>().AsNoTracking();

        public IQueryable<T> TrackedQuery => context.Set<T>().AsTracking();

        public Repository(OrderingContext context)
        {
            this.context = context;
        }

        public T Add(T entity)
        {
            if (entity is AppEntity appEntity)
            {
                if (appEntity.IsTransient())
                {
                    return context.Add(entity).Entity;
                }
            }
            else
            {
                return context.Add(entity).Entity;
            }

            return entity;
        }

        public T Update(T entity)
        {
            return context.Update(entity).Entity;
        }

        public T Delete(T entity, bool physical = false)
        {
            if (!physical && entity is ISoftDeleteEntity softDeleteEntity)
            {
                return context.SoftRemove(softDeleteEntity).Entity as T;
            }
            else
            {
                return context.Remove(entity).Entity;
            }
        }

        public abstract Task<T> FindByIdAsync(TKey id);
    }
}
