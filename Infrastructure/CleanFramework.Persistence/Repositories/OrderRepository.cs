﻿using CleanFramework.Common.DependencyInjection;
using CleanFramework.Domain.Aggregates.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Persistence.Repositories
{
    public class OrderRepository :
        Repository<Order, int>,
        IOrderRepository,
        IScopedService
    {
        public OrderRepository(OrderingContext context) : base(context)
        {
        }

        public override async Task<Order> FindByIdAsync(int id)
        {
            var order = await context.Order
                .Include(x => x.Address)
                .SingleOrDefaultAsync(o => o.Id == id);

            if (order == null)
            {
                order = context.Order
                    .Local
                    .SingleOrDefault(o => o.Id == id);
            }

            if (order != null)
            {
                var orderEntry = context.Entry(order);
                await orderEntry.Collection(i => i.OrderItems).LoadAsync();
                await orderEntry.Reference(i => i.OrderStatus).LoadAsync();
            }

            return order;
        }
    }
}
