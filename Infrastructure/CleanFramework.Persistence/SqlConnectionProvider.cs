﻿using CleanFramework.Application.Common.Services;
using CleanFramework.Common.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace CleanFramework.Persistence
{
    public class SqlConnectionProvider : ISqlConnectionProvider, ISingletonService
    {
        private IConfiguration _configuration;

        public SqlConnectionProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SqlConnection CreateConnection()
        {
            var connStr = _configuration.GetConnectionString(nameof(OrderingContext));
            return new SqlConnection(connStr);
        }
    }
}
