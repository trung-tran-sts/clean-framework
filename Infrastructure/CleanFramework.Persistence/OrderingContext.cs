﻿using CleanFramework.Application.Common.Identity;
using CleanFramework.Common.Mediator;
using CleanFramework.Domain.Aggregates.BuyerAggregate;
using CleanFramework.Domain.Aggregates.OrderAggregate;
using CleanFramework.Domain.Base;
using CleanFramework.Persistence.EntityConfigs;
using CleanFramework.Persistence.Mediator;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Persistence
{
    public class OrderingContext : DbContext, IUnitOfWork
    {
        // [Important] It is acceptable to comment obsolete migration logics (e.g, Changes in properties)
        public static readonly List<Func<OrderingContext, IServiceProvider, Task>> MigrationSeedingActions
            = new List<Func<OrderingContext, IServiceProvider, Task>>();

        private readonly IMediator _mediator;
        private readonly ICurrentUserProvider _currentUserProvider;

        public OrderingContext(ICurrentUserProvider currentUserProvider)
        {
            _currentUserProvider = currentUserProvider;
        }

        public OrderingContext(DbContextOptions<OrderingContext> options,
            ICurrentUserProvider currentUserProvider) : base(options)
        {
            _currentUserProvider = currentUserProvider;
        }

        public OrderingContext(DbContextOptions<OrderingContext> options,
            IMediator mediator,
            ICurrentUserProvider currentUserProvider) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _currentUserProvider = currentUserProvider;
            System.Diagnostics.Debug.WriteLine("OrderingContext::ctor ->" + GetHashCode());
        }

        public DbSet<Order> Order { get; set; }
        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<PaymentMethod> PaymentMethod { get; set; }
        public DbSet<Buyer> Buyer { get; set; }
        public DbSet<CardType> CardType { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public IDbContextTransaction CurrentTransaction => Database.CurrentTransaction;
        public bool HasActiveTransaction => Database.CurrentTransaction != null;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var dbContextAssembly = typeof(OrderingContext).Assembly;

            modelBuilder.ApplyConfigurationsFromAssembly(dbContextAssembly);

            modelBuilder.RestrictDeleteBehaviour(fkPredicate:
                fk => fk.DeleteBehavior != DeleteBehavior.Cascade);

            modelBuilder.AddGlobalQueryFilter(new[] { dbContextAssembly });
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            await _mediator.DispatchDomainEventsAsync(this);

            // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
            // performed through the DbContext will be committed
            var result = await SaveChangesAsync(cancellationToken);

            return true;
        }

        #region SaveChanges
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            AuditEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
        #endregion

        public async Task SeedMigrationsAsync(IServiceProvider serviceProvider)
        {
            using (var transaction = await BeginTransactionAsync())
            {
                foreach (var action in MigrationSeedingActions)
                {
                    await action(this, serviceProvider);
                }

                MigrationSeedingActions.Clear();

                await transaction.CommitAsync();
            }
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync(
            CancellationToken cancellationToken = default)
        {
            if (HasActiveTransaction) return null;

            return await Database.BeginTransactionAsync(cancellationToken);
        }

        private void AuditEntities()
        {
            var hasChanges = ChangeTracker.HasChanges();
            if (!hasChanges) return;

            var entries = ChangeTracker.Entries()
                .Where(o => o.State == EntityState.Modified ||
                    o.State == EntityState.Added).ToArray();

            foreach (var entry in entries)
            {
                var entity = entry.Entity;

                switch (entry.State)
                {
                    case EntityState.Modified:
                        {
                            var isSoftDeleted = false;

                            if (entity is ISoftDeleteEntity softDeleteEntity)
                            {
                                if (softDeleteEntity.IsDeleted)
                                {
                                    if (softDeleteEntity.DeletedTime != null)
                                        throw new InvalidOperationException($"{nameof(entity)} is already deleted");

                                    softDeleteEntity.DeletedTime = DateTimeOffset.UtcNow;
                                    isSoftDeleted = true;

                                    if (entity is ISoftDeleteEntity<int?> userSoftDeleteEntity)
                                    {
                                        userSoftDeleteEntity.DeletorId = _currentUserProvider?.UserId;
                                    }
                                }
                            }

                            if (!isSoftDeleted && entity is IAuditableEntity auditableEntity)
                            {
                                auditableEntity.LastModifiedTime = DateTimeOffset.UtcNow;

                                if (!isSoftDeleted && entity is IAuditableEntity<int?> userAuditableEntity)
                                {
                                    userAuditableEntity.LastModifyUserId = _currentUserProvider?.UserId;
                                }
                            }
                            break;
                        }
                    case EntityState.Added:
                        {
                            if (entity is IAuditableEntity auditableEntity)
                            {
                                auditableEntity.CreatedTime = DateTimeOffset.UtcNow;

                                if (entity is IAuditableEntity<int?> userAuditableEntity)
                                {
                                    userAuditableEntity.CreatorId = _currentUserProvider?.UserId;
                                }
                            }
                            break;
                        }
                }
            }
        }
    }

    public class OrderingContextDesignFactory : IDesignTimeDbContextFactory<OrderingContext>
    {
        public OrderingContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<OrderingContext>()
                .UseSqlServer("Server=localhost;Database=CleanFrameworkOrdering;Trusted_Connection=True;MultipleActiveResultSets=true");

            return new OrderingContext(optionsBuilder.Options, new NullMediator(), new NullCurrentUserProvider());
        }
    }
}
