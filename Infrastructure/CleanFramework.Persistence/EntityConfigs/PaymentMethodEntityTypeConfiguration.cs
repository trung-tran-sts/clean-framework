﻿using CleanFramework.Domain.Aggregates.BuyerAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanFramework.Persistence.EntityConfigs
{
    class PaymentMethodEntityTypeConfiguration : IEntityTypeConfiguration<PaymentMethod>
    {
        const string PaymentMethodSeq = nameof(PaymentMethodSeq);

        public void Configure(EntityTypeBuilder<PaymentMethod> builder)
        {
            builder.Property(o => o.Id).UseHiLo(PaymentMethodSeq);

            builder
                .Property(o => o.CardHolderName)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .Property(o => o.Alias)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .Property(o => o.CardNumber)
                .HasMaxLength(25)
                .IsRequired();

            builder
                .Property(o => o.Expiration)
                .HasMaxLength(25);

            builder.HasOne(p => p.CardType)
                .WithMany()
                .HasForeignKey(o => o.CardTypeId);
        }
    }
}
