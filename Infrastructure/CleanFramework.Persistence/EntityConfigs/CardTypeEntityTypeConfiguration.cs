﻿using CleanFramework.Domain.Aggregates.BuyerAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanFramework.Persistence.EntityConfigs
{
    class CardTypeEntityTypeConfiguration : IEntityTypeConfiguration<CardType>
    {
        public void Configure(EntityTypeBuilder<CardType> builder)
        {
            builder.Property(ct => ct.Id)
                .ValueGeneratedNever();

            builder.Property(ct => ct.Name)
                .HasMaxLength(200)
                .IsRequired();

            builder.HasData(Domain.Base.Enumeration.GetAll<CardType>());
        }
    }
}
