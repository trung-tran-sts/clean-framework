﻿using CleanFramework.Domain.Aggregates.BuyerAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanFramework.Persistence.EntityConfigs
{
    class BuyerEntityTypeConfiguration : IEntityTypeConfiguration<Buyer>
    {
        const string BuyerSeq = nameof(BuyerSeq);

        public void Configure(EntityTypeBuilder<Buyer> builder)
        {
            builder.Property(o => o.Id).UseHiLo(BuyerSeq);

            builder.Property(b => b.Name);

            builder.HasMany(b => b.PaymentMethods)
               .WithOne()
               .HasForeignKey(o => o.BuyerId)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
