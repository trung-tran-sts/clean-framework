﻿using CleanFramework.Domain.Aggregates.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanFramework.Persistence.EntityConfigs
{
    class OrderItemEntityTypeConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        const string OrderItemSeq = nameof(OrderItemSeq);

        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.Property(o => o.Id).UseHiLo(OrderItemSeq);
        }
    }
}
