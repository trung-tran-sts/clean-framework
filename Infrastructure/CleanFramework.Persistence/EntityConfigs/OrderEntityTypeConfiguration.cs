﻿using CleanFramework.Domain.Aggregates.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanFramework.Persistence.EntityConfigs
{
    class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
    {
        const string OrderSeq = nameof(OrderSeq);

        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(o => o.Id).UseHiLo(OrderSeq);

            //Address value object persisted as owned entity type supported since EF Core 2.0
            builder
                .OwnsOne(o => o.Address, a =>
                {
                    // Explicit configuration of the shadow key property in the owned type 
                    // as a workaround for a documented issue in EF Core 5: https://github.com/dotnet/efcore/issues/20740
                    a.Property(o => o.OrderId).UseHiLo(OrderSeq);
                    a.WithOwner();
                });

            builder.HasOne(o => o.PaymentMethod)
                .WithMany()
                .HasForeignKey(o => o.PaymentMethodId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(o => o.Buyer)
                .WithMany()
                .HasForeignKey(o => o.BuyerId);

            builder.HasOne(o => o.OrderStatus)
                .WithMany()
                .HasForeignKey(o => o.OrderStatusId);

            builder.HasMany(o => o.OrderItems)
                .WithOne()
                .HasForeignKey(o => o.OrderId);
        }
    }
}
