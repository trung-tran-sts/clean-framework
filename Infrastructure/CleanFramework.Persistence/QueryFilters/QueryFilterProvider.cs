﻿using Microsoft.EntityFrameworkCore.Metadata;

namespace CleanFramework.Persistence.QueryFilters
{
    public interface IQueryFilterProvider
    {
        string ProvideMethodName { get; }
        bool CanApply(IMutableEntityType eType);
    }
}
