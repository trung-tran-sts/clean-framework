﻿using CleanFramework.Domain.Base;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq.Expressions;

namespace CleanFramework.Persistence.QueryFilters
{
    public class NotDeletedQueryFilter : IQueryFilterProvider
    {
        public string ProvideMethodName => nameof(CreateFilter);

        public bool CanApply(IMutableEntityType eType)
            => typeof(ISoftDeleteEntity).IsAssignableFrom(eType.ClrType);

        public Expression<Func<TEntity, bool>> CreateFilter<TEntity>() where TEntity : ISoftDeleteEntity
            => (o) => !o.IsDeleted;
    }
}
