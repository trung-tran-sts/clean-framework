﻿using Serilog;
using Serilog.Configuration;
using System;

namespace CleanFramework.Infrastructure.Logging
{
    public static class LoggerConfigurationExtensions
    {
        public static LoggerConfiguration WithFileLoggerFilter(this LoggerFilterConfiguration filter)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter));

            return filter.With<FileLoggerFilter>();
        }
    }
}
