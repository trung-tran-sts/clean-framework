﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Linq;

namespace CleanFramework.Infrastructure.Logging
{
    public class FileLoggerFilter : ILogEventFilter
    {
        public bool IsEnabled(LogEvent logEvent)
        {
            var acceptedLogLevel = new[] { LogEventLevel.Error, LogEventLevel.Fatal };
            return acceptedLogLevel.Contains(logEvent.Level);
        }
    }
}
