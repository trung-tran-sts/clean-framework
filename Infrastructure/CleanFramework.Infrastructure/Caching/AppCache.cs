﻿using CleanFramework.Application.Common.Services;
using EasyCaching.Core;
using System;
using System.Threading.Tasks;

namespace CleanFramework.Infrastructure.Caching
{
    public class AppCache : IAppCache
    {
        static readonly TimeSpan DefaultExpiration = TimeSpan.FromDays(365);

        private readonly IEasyCachingProvider _easyCachingProvider;

        public AppCache(IEasyCachingProvider easyCachingProvider)
        {
            _easyCachingProvider = easyCachingProvider;
        }

        public Task<bool> TrySetAsync<T>(string cacheKey, T cacheValue, TimeSpan? expiration = null)
        {
            return _easyCachingProvider.TrySetAsync(cacheKey, cacheValue, expiration ?? DefaultExpiration);
        }

        public async Task<T> GetOrAddAsync<T>(string cacheKey, Func<Task<T>> createFunc, TimeSpan? expiration = null)
        {
            var cacheValue = await _easyCachingProvider.GetAsync(cacheKey, createFunc, expiration ?? DefaultExpiration);
            return cacheValue.HasValue ? cacheValue.Value : default;
        }

        public async Task<T> GetAsync<T>(string cacheKey)
        {
            var cacheValue = await _easyCachingProvider.GetAsync<T>(cacheKey);
            return cacheValue.HasValue ? cacheValue.Value : default;
        }
    }
}
