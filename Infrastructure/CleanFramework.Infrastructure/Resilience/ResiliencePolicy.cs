﻿using Polly;
using Polly.Contrib.WaitAndRetry;
using Polly.Registry;
using Polly.Retry;
using System;

namespace CleanFramework.Infrastructure.Resilience
{
    public static class ResiliencePolicy
    {
        public const string CommonRetryPolicy = nameof(CommonRetryPolicy);

        // High throughput scenario: https://github.com/App-vNext/Polly/wiki/Avoiding-cache-repopulation-request-storms
        public static PolicyRegistry InitPolly(IServiceProvider serviceProvider)
        {
            PolicyRegistry registry = new PolicyRegistry();

            var delay = Backoff.DecorrelatedJitterBackoffV2(medianFirstRetryDelay: TimeSpan.FromSeconds(2), retryCount: 3);
            AsyncRetryPolicy commonRetry = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(delay, onRetry: (exception, delay, count, context) => { });
            registry.Add(CommonRetryPolicy, commonRetry);

            return registry;
        }
    }
}
