﻿using System;
using System.Collections.Generic;

namespace CleanFramework.Common.Collections
{
    public static class EnumerableExtensions
    {
        public static T[] SubSet<T>(this T[] arr, int fromIdx, int length)
        {
            var newArr = new T[length];

            Array.Copy(arr, fromIdx, newArr, 0, length);

            return newArr;
        }

        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> collection, int size)
        {
            var chunkList = new List<IEnumerable<T>>();
            var currentChunk = new List<T>();
            var idx = 0;

            foreach (var item in collection)
            {
                currentChunk.Add(item);
                idx += 1;
                if (idx % size == 0)
                {
                    chunkList.Add(currentChunk);
                    currentChunk = new List<T>();
                }
            }

            if (currentChunk.Count > 0) chunkList.Add(currentChunk);

            return chunkList;
        }
    }
}
