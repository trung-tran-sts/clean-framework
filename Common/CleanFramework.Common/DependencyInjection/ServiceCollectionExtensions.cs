﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace CleanFramework.Common.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ScanServices(this IServiceCollection services,
            Func<Assembly, bool> predicate)
        {
            return services.Scan(scan => (predicate != null
                    ? scan.FromApplicationDependencies(predicate)
                    : scan.FromApplicationDependencies())
                .AddClasses(classes => classes.AssignableTo<ITransientService>())
                .AsSelfWithInterfaces()
                .WithTransientLifetime()

                .AddClasses(classes => classes.AssignableTo<IScopedService>())
                .AsSelfWithInterfaces()
                .WithScopedLifetime()

                .AddClasses(classes => classes.AssignableTo<ISingletonService>())
                .AsSelfWithInterfaces()
                .WithSingletonLifetime());
        }
    }
}
