﻿namespace CleanFramework.Common.DependencyInjection
{
    public interface ITransientService
    {
    }

    public interface IScopedService
    {
    }

    public interface ISingletonService
    {
    }
}
