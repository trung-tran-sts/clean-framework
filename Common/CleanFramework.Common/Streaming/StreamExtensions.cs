﻿using System.IO;
using System.Threading.Tasks;

namespace CleanFramework.Common.Streaming
{
    public static class StreamExtensions
    {
        public static Task<string> ReadAsStringAsync(this Stream stream)
        {
            return new StreamReader(stream).ReadToEndAsync();
        }
    }
}
