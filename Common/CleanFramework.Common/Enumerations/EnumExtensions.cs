﻿using EnumsNET;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CleanFramework.Common.Enumerations
{
    public static class EnumExtensions
    {
        public static string GetDisplayName<T>(this T @enum) where T : struct, Enum
        {
            var attr = @enum.GetAttributes();
            var displayName = attr.OfType<DisplayNameAttribute>().FirstOrDefault()?.DisplayName;

            if (displayName == null)
            {
                displayName = attr.OfType<DisplayAttribute>().FirstOrDefault()?.Name;
            }

            return displayName;
        }

        public static string GetDescription<T>(this T @enum) where T : struct, Enum
        {
            var attr = @enum.GetAttributes();
            var description = attr.OfType<DescriptionAttribute>().FirstOrDefault()?.Description;

            if (description == null)
            {
                description = attr.OfType<DisplayAttribute>().FirstOrDefault()?.Description;
            }

            return description;
        }

        public static DisplayAttribute GetDisplay<T>(this T @enum) where T : struct, Enum
        {
            var displayAttr = @enum.GetAttributes()
                .OfType<DisplayAttribute>()
                .SingleOrDefault();

            return displayAttr;
        }

        public static string GetName<T>(this T enumVal) where T : struct, Enum
        {
            return Enums.GetName(enumVal);
        }

        public static string ToStringF(this Enum enumVal)
        {
            return enumVal.ToString("F");
        }

        public static string ToStringG(this Enum enumVal)
        {
            return enumVal.ToString("G");
        }
    }
}
