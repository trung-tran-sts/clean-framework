﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace CleanFramework.Common.Web.Requests
{
    public static class RequestBufferingMiddleware
    {
        public static IApplicationBuilder UseRequestBuffering(this IApplicationBuilder app)
        {
            return app.Use(async (context, next) =>
            {
                context.Request.EnableBuffering();
                await next();
            });
        }
    }

}
