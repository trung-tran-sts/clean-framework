﻿namespace CleanFramework.Common.Logging
{
    internal static class Properties
    {
        public const string CallerMemberName = nameof(CallerMemberName);
        public const string CallerFilePath = nameof(CallerFilePath);
        public const string CallerLineNumber = nameof(CallerLineNumber);
    }
}
