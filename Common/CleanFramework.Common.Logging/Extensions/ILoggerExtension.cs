﻿using Serilog;
using System.Runtime.CompilerServices;

namespace CleanFramework.Common.Logging.Extensions
{
    public static class ILoggerExtensions
    {
        public static ILogger CallerContext(this ILogger logger,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0)
        {
            return logger.ForContext(Properties.CallerMemberName, memberName)
                .ForContext(Properties.CallerFilePath, fileName)
                .ForContext(Properties.CallerLineNumber, lineNumber);
        }
    }
}
