﻿using CleanFramework.Domain.Base;

namespace CleanFramework.Domain.Aggregates.BuyerAggregate
{
    public interface IBuyerRepository : IRepository<Buyer, int>
    {
    }
}
