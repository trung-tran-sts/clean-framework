﻿using CleanFramework.Domain.Base;
using CleanFramework.Domain.Exceptions;
using System;

namespace CleanFramework.Domain.Aggregates.BuyerAggregate
{
    public class PaymentMethod : AppFullAuditableEntity
    {
        public string Alias { get; private set; }
        public string CardNumber { get; private set; }
        public string SecurityNumber { get; private set; }
        public DateTimeOffset Expiration { get; private set; }
        public string CardHolderName { get; private set; }
        public int CardTypeId { get; private set; }
        public CardType CardType { get; private set; }
        public int BuyerId { get; private set; }

        protected PaymentMethod() { }

        public PaymentMethod(int cardTypeId, string alias, string cardNumber, string securityNumber, string cardHolderName, DateTimeOffset expiration)
        {
            CardNumber = !string.IsNullOrWhiteSpace(cardNumber) ? cardNumber : throw new InvalidDomainModelException(nameof(cardNumber));
            SecurityNumber = !string.IsNullOrWhiteSpace(securityNumber) ? securityNumber : throw new InvalidDomainModelException(nameof(securityNumber));
            CardHolderName = !string.IsNullOrWhiteSpace(cardHolderName) ? cardHolderName : throw new InvalidDomainModelException(nameof(cardHolderName));
            Expiration = expiration >= DateTimeOffset.UtcNow ? expiration : throw new InvalidDomainModelException(nameof(expiration));
            Alias = alias;
            CardTypeId = cardTypeId;
        }

        public bool IsEqualTo(int cardTypeId, string cardNumber, DateTimeOffset expiration)
        {
            return CardTypeId == cardTypeId
                && CardNumber == cardNumber
                && Expiration == expiration;
        }
    }
}
