﻿using CleanFramework.Domain.Exceptions;
using Microsoft.Extensions.Logging;

namespace CleanFramework.Domain.Aggregates.OrderAggregate.Exceptions
{
    public class ProductNotInOrderException : BaseException
    {
        public ProductNotInOrderException()
            : base(ResultCode.Order_ProductNotFoundInOrder,
                  message: null,
                  data: null,
                  logLevel: LogLevel.Error)
        {
        }
    }
}
