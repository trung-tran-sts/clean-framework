﻿using CleanFramework.Domain.Exceptions;
using Microsoft.Extensions.Logging;

namespace CleanFramework.Domain.Aggregates.OrderAggregate.Exceptions
{
    public class OrderTotalLowerThanDiscountException : BaseException
    {
        public OrderTotalLowerThanDiscountException()
            : base(ResultCode.Order_TotalLowerThanDiscount,
                  message: null,
                  data: null,
                  logLevel: LogLevel.Error)
        {
        }
    }
}
