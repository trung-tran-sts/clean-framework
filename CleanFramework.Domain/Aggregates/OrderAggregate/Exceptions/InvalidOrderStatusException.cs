﻿using CleanFramework.Domain.Exceptions;
using Microsoft.Extensions.Logging;

namespace CleanFramework.Domain.Aggregates.OrderAggregate.Exceptions
{
    public class InvalidOrderStatusException : BaseException
    {
        public InvalidOrderStatusException()
            : base(ResultCode.Order_InvalidOrderStatus,
                  message: null,
                  data: null,
                  logLevel: LogLevel.Error)
        {
        }
    }
}
