﻿using CleanFramework.Domain.Exceptions;
using Microsoft.Extensions.Logging;

namespace CleanFramework.Domain.Aggregates.OrderAggregate.Exceptions
{
    public class InvalidDiscountException : BaseException
    {
        public InvalidDiscountException()
            : base(ResultCode.Order_InvalidDiscount,
                  message: null,
                  data: null,
                  logLevel: LogLevel.Error)
        {
        }
    }
}
