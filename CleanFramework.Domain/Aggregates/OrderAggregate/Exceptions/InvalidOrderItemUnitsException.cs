﻿using CleanFramework.Domain.Exceptions;
using Microsoft.Extensions.Logging;

namespace CleanFramework.Domain.Aggregates.OrderAggregate.Exceptions
{
    public class InvalidOrderItemUnitsException : BaseException
    {
        public InvalidOrderItemUnitsException()
            : base(ResultCode.Order_InvalidUnits,
                  message: null,
                  data: null,
                  logLevel: LogLevel.Error)
        {
        }
    }
}
