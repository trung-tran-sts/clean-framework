﻿using CleanFramework.Common.DependencyInjection;
using System;

namespace CleanFramework.Domain.Aggregates.OrderAggregate.Factories
{
    public interface IOrderFactory
    {
        Order CreateNewOrder(Address address,
            int cardTypeId, string cardNumber, string cardSecurityNumber, string cardHolderName,
            DateTimeOffset cardExpiration, string buyerName, int? buyerId = null);

        Order CreateNewDraftOrder(int buyerId);
    }

    public class OrderFactory : IOrderFactory, ISingletonService
    {
        public Order CreateNewOrder(Address address,
            int cardTypeId, string cardNumber, string cardSecurityNumber, string cardHolderName,
            DateTimeOffset cardExpiration, string buyerName, int? buyerId = null)
        {
            var order = new Order(address, cardTypeId, cardNumber, cardSecurityNumber,
                cardHolderName, cardExpiration,
                buyerName, buyerId);

            return order;
        }

        public Order CreateNewDraftOrder(int buyerId)
        {
            var order = Order.NewDraftOrder(buyerId);
            return order;
        }
    }
}
