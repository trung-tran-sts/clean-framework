﻿using CleanFramework.Domain.Base;

namespace CleanFramework.Domain.Aggregates.OrderAggregate
{
    public interface IOrderRepository : IRepository<Order, int>
    {
    }
}
