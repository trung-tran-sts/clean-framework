﻿using CleanFramework.Domain.Aggregates.BuyerAggregate;
using CleanFramework.Domain.Aggregates.OrderAggregate.Exceptions;
using CleanFramework.Domain.Base;
using CleanFramework.Domain.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CleanFramework.Domain.Aggregates.OrderAggregate
{
    public class Order : AppFullAuditableEntity, IAggregateRoot
    {
        public int OrderStatusId { get; private set; }
        public int BuyerId { get; private set; }
        public int PaymentMethodId { get; private set; }
        public Address Address { get; private set; }
        public DateTimeOffset OrderDate { get; private set; }
        public OrderStatus OrderStatus { get; private set; }
        public string Description { get; private set; }
        public Buyer Buyer { get; private set; }
        public PaymentMethod PaymentMethod { get; private set; }

        private readonly List<OrderItem> _orderItems;
        private IEnumerable<OrderItem> ValidOrderItems => _orderItems.AsReadOnly().IsNotDeleted();
        public IEnumerable<OrderItem> OrderItems => ValidOrderItems;

        protected Order()
        {
            _orderItems = new List<OrderItem>();
        }

        internal Order(int buyerId) : this()
        {
            BuyerId = buyerId;
        }

        internal Order(Address address, int cardTypeId, string cardNumber, string cardSecurityNumber,
            string cardHolderName, DateTimeOffset cardExpiration,
            string buyerName, int? buyerId = null) : this()
        {
            Address = address;
            BuyerId = buyerId ?? default;
            OrderStatus = OrderStatus.Submitted;
            OrderStatusId = OrderStatus.Id;
            OrderDate = DateTimeOffset.UtcNow;

            // Add the OrderStarterDomainEvent to the domain events collection 
            // to be raised/dispatched when comitting changes into the Database [ After DbContext.SaveChanges() ]
            AddOrderStartedDomainEvent(buyerName, cardTypeId, cardNumber,
                cardSecurityNumber, cardHolderName, cardExpiration,
                buyerId);
        }

        internal static Order NewDraftOrder(int buyerId)
        {
            var order = new Order(buyerId);
            order.OrderStatus = OrderStatus.Submitted;
            order.OrderStatusId = order.OrderStatus.Id;

            return order;
        }

        // DDD Patterns comment
        // This Order AggregateRoot's method "AddOrderitem()" should be the only way to add Items to the Order,
        // so any behavior (discounts, etc.) and validations are controlled by the AggregateRoot 
        // in order to maintain consistency between the whole Aggregate. 
        public void AddOrderItem(string productName, decimal unitPrice, decimal discount, string pictureUrl, int units = 1)
        {
            var existingOrderForProduct = ValidOrderItems.Where(o => o.ProductName == productName)
                .SingleOrDefault();

            if (existingOrderForProduct != null)
            {
                //if previous line exist modify it with higher discount  and units..
                if (discount > existingOrderForProduct.Discount)
                {
                    existingOrderForProduct.SetNewDiscount(discount);
                }

                existingOrderForProduct.AddUnits(units);
            }
            else
            {
                //add validated new order item
                var orderItem = new OrderItem(productName, unitPrice, discount, pictureUrl, units);
                _orderItems.Add(orderItem);
            }
        }

        public void RemoveOrderItem(string productName)
        {
            var existingItemForProduct = ValidOrderItems.Where(o => o.ProductName == productName)
                .SingleOrDefault();

            if (existingItemForProduct != null)
            {
                // [Important] Physical remove
                //_orderItems.Remove(existingItemForProduct);

                // [Important] Soft remove
                existingItemForProduct.IsDeleted = true;

                // [Important] raise events if necessary
            }
            else
            {
                throw new ProductNotInOrderException();
            }
        }

        public void SetAddress(Address address)
        {
            // [TODO] Address validation
            Address = address;
        }

        public void SetPaymentId(int id)
        {
            PaymentMethodId = id;
        }

        public void SetBuyerId(int id)
        {
            BuyerId = id;
        }

        public decimal GetTotal()
        {
            return ValidOrderItems.Sum(oItem => oItem.GetTotal());
        }

        public static Expression<Func<Order, decimal>> GetTotalExpr =>
            order => order.OrderItems.Sum(oI => oI.Units * oI.UnitPrice);

        private void AddOrderStartedDomainEvent(string buyerName, int cardTypeId, string cardNumber,
                string cardSecurityNumber, string cardHolderName, DateTimeOffset cardExpiration,
                int? buyerId)
        {
            var orderStartedDomainEvent = new OrderStartedDomainEvent(this, buyerName, cardTypeId,
                cardNumber, cardSecurityNumber,
                cardHolderName, cardExpiration,
                buyerId);

            AddDomainEvent(orderStartedDomainEvent);
        }
    }
}
