﻿using CleanFramework.Domain.Aggregates.OrderAggregate.Exceptions;
using CleanFramework.Domain.Base;

namespace CleanFramework.Domain.Aggregates.OrderAggregate
{
    public class OrderItem : AppFullAuditableEntity
    {
        public int OrderId { get; private set; }
        public string ProductName { get; private set; }
        public string PictureUrl { get; private set; }
        public decimal UnitPrice { get; private set; }
        public decimal Discount { get; private set; }
        public int Units { get; private set; }

        protected OrderItem() { }

        public OrderItem(string productName, decimal unitPrice,
            decimal discount, string pictureUrl, int units = 1)
        {
            if (units <= 0)
            {
                throw new InvalidOrderItemUnitsException();
            }

            if (unitPrice * units < discount)
            {
                throw new OrderTotalLowerThanDiscountException();
            }

            ProductName = productName;
            UnitPrice = unitPrice;
            Discount = discount;
            Units = units;
            PictureUrl = pictureUrl;
        }

        public void SetNewDiscount(decimal discount)
        {
            if (discount < 0)
            {
                throw new InvalidDiscountException();
            }

            Discount = discount;
        }

        public void AddUnits(int units)
        {
            if (units < 0)
            {
                throw new InvalidOrderItemUnitsException();
            }

            Units += units;
        }

        public void RemoveUnits(int units)
        {
            if (units < 0)
            {
                throw new InvalidOrderItemUnitsException();
            }

            if (units > Units)
            {
                throw new InvalidOrderItemUnitsException();
            }

            Units += units;
        }

        public decimal GetTotal()
        {
            return Units * UnitPrice;
        }
    }
}
