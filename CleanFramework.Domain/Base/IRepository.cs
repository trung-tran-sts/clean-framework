﻿using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Domain.Base
{
    public interface IRepository<T, TKey> where T : class, IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
        IQueryable<T> NoTrackedQuery { get; }
        IQueryable<T> TrackedQuery { get; }
        Task<T> FindByIdAsync(TKey id);
        T Add(T entity);
        T Update(T entity);
        T Delete(T entity, bool physical = false);
    }
}
