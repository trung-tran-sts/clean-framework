﻿using System;

namespace CleanFramework.Domain.Base
{
    public abstract class AppEntity : DomainEntity
    {
        private int? _requestedHashCode;
        private int _id;
        public virtual int Id
        {
            get
            {
                return _id;
            }
            protected set
            {
                _id = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is AppFullAuditableEntity))
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            if (this.GetType() != obj.GetType())
                return false;

            AppFullAuditableEntity item = (AppFullAuditableEntity)obj;

            if (item.IsTransient() || this.IsTransient())
                return false;
            else
                return item.Id == this.Id;
        }

        public bool IsTransient()
        {
            return this.Id == default(Int32);
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                    _requestedHashCode = this.Id.GetHashCode() ^ 31; // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)

                return _requestedHashCode.Value;
            }
            else
                return base.GetHashCode();

        }
        public static bool operator ==(AppEntity left, AppEntity right)
        {
            if (Object.Equals(left, null))
                return (Object.Equals(right, null)) ? true : false;
            else
                return left.Equals(right);
        }

        public static bool operator !=(AppEntity left, AppEntity right)
        {
            return !(left == right);
        }

    }

    public abstract class AppFullAuditableEntity : AppEntity, IAuditableEntity<int?>, ISoftDeleteEntity<int?>
    {
        public int? DeletorId { get; set; }
        public DateTimeOffset? DeletedTime { get; set; }
        public bool IsDeleted { get; set; }
        public int? CreatorId { get; set; }
        public int? LastModifyUserId { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
        public DateTimeOffset? LastModifiedTime { get; set; }
    }
}
