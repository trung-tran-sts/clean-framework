﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Domain.Base
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// [Important] Should dispatch domain events and add auditing info
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
