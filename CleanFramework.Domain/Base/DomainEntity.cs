﻿using MediatR;
using System.Collections.Generic;

namespace CleanFramework.Domain.Base
{
    public abstract class DomainEntity
    {
        private List<INotification> _domainEvents;

        public IReadOnlyList<INotification> GetDomainEvents()
        {
            return _domainEvents?.AsReadOnly();
        }

        public void AddDomainEvent(INotification eventItem)
        {
            _domainEvents = _domainEvents ?? new List<INotification>();
            _domainEvents.Add(eventItem);
        }

        public void RemoveDomainEvent(INotification eventItem)
        {
            _domainEvents?.Remove(eventItem);
        }

        public void ClearDomainEvents()
        {
            _domainEvents?.Clear();
        }
    }
}
