﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CleanFramework.Domain
{
    public enum ResultCode
    {
        #region Global
        Global = 10000,

        [Display(GroupName = nameof(Global))]
        [Description("Object result")]
        Global_ObjectResult = Global + 1,

        [Display(GroupName = nameof(Global))]
        [Description("Resource not found")]
        Global_ResourceNotFound = Global + 2,

        [Display(GroupName = nameof(Global))]
        [Description("Bad request")]
        Global_BadRequest = Global + 3,

        [Display(GroupName = nameof(Global))]
        [Description("Unknown error")]
        Global_UnknownError = Global + 4,
        #endregion

        #region Order
        Order = 11000,

        [Display(GroupName = nameof(Order))]
        [Description("Invalid number of units")]
        Order_InvalidUnits = Order + 1,

        [Display(GroupName = nameof(Order))]
        [Description("The total of order item is lower than applied discount")]
        Order_TotalLowerThanDiscount = Order + 2,

        [Display(GroupName = nameof(Order))]
        [Description("Invalid discount")]
        Order_InvalidDiscount = Order + 3,

        [Display(GroupName = nameof(Order))]
        [Description("Invalid order status value")]
        Order_InvalidOrderStatus = Order + 4,

        [Display(GroupName = nameof(Order))]
        [Description("Specified product not found in order")]
        Order_ProductNotFoundInOrder = Order + 5,
        #endregion
    }
}
