﻿using Microsoft.Extensions.Logging;

namespace CleanFramework.Domain.Exceptions
{
    public class NotFoundException : BaseException
    {
        public NotFoundException(object data = null, string message = null)
            : base(ResultCode.Global_ResourceNotFound, message, data, LogLevel.Information)
        {
        }
    }
}
