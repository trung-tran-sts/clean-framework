﻿using CleanFramework.Common.Enumerations;
using Microsoft.Extensions.Logging;
using System;

namespace CleanFramework.Domain.Exceptions
{
    public abstract class BaseException : Exception
    {
        public BaseException(ResultCode resultCode,
            string message = null,
            object data = null,
            LogLevel logLevel = LogLevel.Error)
        {
            Code = resultCode;
            DataObject = data;
            _message = message;
            LogLevel = logLevel;
        }

        private string _message;
        public override string Message => _message ??
            Code.GetDescription() ??
            Code.GetDisplayName() ??
            Code.GetName();

        public object DataObject { get; }
        public ResultCode Code { get; }
        public LogLevel LogLevel { get; }
    }
}
