﻿namespace CleanFramework.Domain.Exceptions
{
    public class InvalidDomainModelException : BaseException
    {
        public InvalidDomainModelException(params string[] messages)
            : base(ResultCode.Global_BadRequest, data: messages ?? new string[0])
        {
        }
    }
}
