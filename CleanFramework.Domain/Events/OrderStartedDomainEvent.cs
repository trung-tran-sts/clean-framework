﻿using CleanFramework.Domain.Aggregates.OrderAggregate;
using MediatR;
using System;

namespace CleanFramework.Domain.Events
{
    /// <summary>
    /// Event used when an order is created
    /// </summary>
    public class OrderStartedDomainEvent : INotification
    {
        public int? BuyerId { get; }
        public string BuyerName { get; }
        public int CardTypeId { get; }
        public string CardNumber { get; }
        public string CardSecurityNumber { get; }
        public string CardHolderName { get; }
        public DateTimeOffset CardExpiration { get; }
        public Order Order { get; }

        public OrderStartedDomainEvent(Order order, string buyerName,
                                       int cardTypeId, string cardNumber,
                                       string cardSecurityNumber, string cardHolderName,
                                       DateTimeOffset cardExpiration,
                                       int? buyerId = null)
        {
            Order = order;
            BuyerName = buyerName;
            BuyerId = buyerId;
            CardTypeId = cardTypeId;
            CardNumber = cardNumber;
            CardSecurityNumber = cardSecurityNumber;
            CardHolderName = cardHolderName;
            CardExpiration = cardExpiration;
        }
    }
}
