﻿using CleanFramework.Domain.Events;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.DomainEventHandlers.OrderStarted
{
    public class SendEmailToCustomerWhenOrderStartedDomainEventHandler : INotificationHandler<OrderStartedDomainEvent>
    {
        public SendEmailToCustomerWhenOrderStartedDomainEventHandler()
        {
        }

        public Task Handle(OrderStartedDomainEvent notification, CancellationToken cancellationToken)
        {
            // [Important] [TODO] Send email logic
            return Task.CompletedTask;
        }
    }
}
