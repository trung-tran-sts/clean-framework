﻿using MediatR;

namespace CleanFramework.Application.Base
{
    public interface IBaseTransactionalCommand { }

    public interface ITransactionalCommand : IBaseTransactionalCommand, IRequest { }

    public interface ITransactionalCommand<T> : IBaseTransactionalCommand, IRequest<T> { }
}
