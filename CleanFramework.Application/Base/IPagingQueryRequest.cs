﻿namespace CleanFramework.Application.Base
{
    public interface IPagingQueryRequest
    {
        string Terms { get; }
        int Skip { get; }
        int? Take { get; }
        bool CanGetAll();
    }
}
