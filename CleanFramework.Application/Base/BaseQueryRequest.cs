﻿namespace CleanFramework.Application.Base
{
    public abstract class BaseQueryRequest : IPagingQueryRequest
    {
        public BaseQueryRequest() { }
        public BaseQueryRequest(string terms,
            int skip, int? take)
        {
            Terms = terms;
            Skip = skip;
            Take = take;
        }

        public string Terms { get; }
        public int Skip { get; }
        public int? Take { get; }

        public abstract bool CanGetAll();
    }
}
