﻿using CleanFramework.Common.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace CleanFramework.Application.Auth.Policies.IsSpecialUser
{
    public class IsSpecialUserHandler :
        AuthorizationHandler<IsSpecialUserRequirement>,
        IScopedService
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsSpecialUserRequirement requirement)
        {
            // [Important] implement some real custom logic
            context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
