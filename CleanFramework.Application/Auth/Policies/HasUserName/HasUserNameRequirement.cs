﻿using Microsoft.AspNetCore.Authorization;

namespace CleanFramework.Application.Auth.Policies.HasUserName
{
    public class HasUserNameRequirement : IAuthorizationRequirement
    {
        public HasUserNameRequirement(string userName)
        {
            UserName = userName;
        }

        public string UserName { get; }
    }
}
