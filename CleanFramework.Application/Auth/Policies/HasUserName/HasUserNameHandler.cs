﻿using CleanFramework.Common.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace CleanFramework.Application.Auth.Policies.HasUserName
{
    public class HasUserNameHandler :
        AuthorizationHandler<HasUserNameRequirement>,
        IScopedService
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasUserNameRequirement requirement)
        {
            var user = context.User;

            if (user.Identity.Name == requirement.UserName)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
