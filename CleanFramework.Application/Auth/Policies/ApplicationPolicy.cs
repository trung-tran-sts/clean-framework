﻿using CleanFramework.Domain.Constants;
using System.Collections.Generic;

namespace CleanFramework.Application.Auth.Policies
{
    public static class ApplicationPolicy
    {
        #region General
        public const string HasRole = nameof(HasRole);
        public const string HasUserName = nameof(HasUserName);
        public const string IsSpecialUser = nameof(IsSpecialUser);
        #endregion

        public static class Admin
        {
            const string Prefix = nameof(Admin) + ":";
            public const string CanDoTaskA = Prefix + nameof(CanDoTaskA);
            public const string CanDoTaskB = Prefix + nameof(CanDoTaskB);
        }

        public static IDictionary<string, string> Entries = new Dictionary<string, string>()
        {
            [Admin.CanDoTaskA] =
                $"{HasRole}:{RoleEnum.Administrator};" +
                $"{HasUserName}:trung.tran;",

            [Admin.CanDoTaskB] =
                $"{IsSpecialUser};"
        };
    }
}
