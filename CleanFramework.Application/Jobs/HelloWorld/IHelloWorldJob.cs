﻿using CleanFramework.Common.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace CleanFramework.Application.Jobs.HelloWorld
{
    public interface IHelloWorldJob
    {
        Task SayHelloWorldAsync(HelloWorldJobData data);
    }

    public class HelloWorldJob : IHelloWorldJob, IScopedService
    {
        public Task SayHelloWorldAsync(HelloWorldJobData data)
        {
            Console.WriteLine($"{DateTimeOffset.UtcNow} - " +
                $"This line is printed from {nameof(HelloWorldJob)}: " +
                $"Hello World, {data.AuthorName}!");
            return Task.CompletedTask;
        }
    }
}
