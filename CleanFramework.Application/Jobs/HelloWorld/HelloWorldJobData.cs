﻿namespace CleanFramework.Application.Jobs.HelloWorld
{
    public class HelloWorldJobData
    {
        public string AuthorName { get; set; }
    }
}
