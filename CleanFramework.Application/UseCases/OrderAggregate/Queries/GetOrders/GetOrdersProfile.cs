﻿using AutoMapper;
using CleanFramework.Domain.Aggregates.OrderAggregate;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrders
{
    public class GetOrdersProfile : Profile
    {
        public GetOrdersProfile()
        {
            CreateMap<Order, OrderSummaryModel>()
                .ForMember(o => o.Total, opt => opt.MapFrom(Order.GetTotalExpr));
        }
    }
}
