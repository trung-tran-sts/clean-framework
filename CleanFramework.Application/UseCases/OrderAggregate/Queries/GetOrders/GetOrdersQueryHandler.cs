﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanFramework.Application.Constants;
using CleanFramework.Domain.Aggregates.OrderAggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrders
{
    public class GetOrdersQueryHandler : IRequestHandler<GetOrdersQuery, IEnumerable<OrderSummaryModel>>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public GetOrdersQueryHandler(IOrderRepository orderRepository,
            IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<OrderSummaryModel>> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
        {
            var query = _orderRepository.NoTrackedQuery;

            if (!string.IsNullOrWhiteSpace(request.Terms))
            {
                query = query.Where(o => o.Id.ToString() == request.Terms
                    || o.Buyer.Name.Contains(request.Terms));
            }

            if (!string.IsNullOrWhiteSpace(request.Country))
            {
                query = query.Where(o => o.Address.Country == request.Country);
            }

            if (request.FromDate != null)
            {
                query = query.Where(o => o.OrderDate >= request.FromDate);
            }

            if (request.ToDate != null)
            {
                query = query.Where(o => o.OrderDate <= request.ToDate);
            }

            if (request.Status != null)
            {
                query = query.Where(o => o.OrderStatus.Id == request.Status);
            }

            query = query.Skip(request.Skip);

            if (request.Take != null || !request.CanGetAll())
            {
                var take = request.Take ?? QueryConstants.DefaultTake;
                query = query.Take(take);
            }

            var orders = await query
                .ProjectTo<OrderSummaryModel>(_mapper.ConfigurationProvider)
                .ToArrayAsync();

            return orders;
        }
    }
}
