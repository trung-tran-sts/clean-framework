﻿using CleanFramework.Application.Base;
using MediatR;
using System;
using System.Collections.Generic;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrders
{
    public class GetOrdersQuery : BaseQueryRequest, IRequest<IEnumerable<OrderSummaryModel>>
    {
        public GetOrdersQuery(string terms, int skip, int? take,
            DateTimeOffset? fromDate, DateTimeOffset? toDate, string country, int? status)
            : base(terms, skip, take)
        {
            FromDate = fromDate;
            ToDate = toDate;
            Country = country;
            Status = status;
        }

        public DateTimeOffset? FromDate { get; }
        public DateTimeOffset? ToDate { get; }
        public string Country { get; }
        public int? Status { get; }

        public override bool CanGetAll() => false;
    }
}
