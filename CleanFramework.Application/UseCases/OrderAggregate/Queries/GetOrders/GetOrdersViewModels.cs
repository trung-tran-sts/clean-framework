﻿using System;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrders
{
    public class OrderSummaryModel
    {
        public int Id { get; set; }
        public DateTimeOffset OrderDate { get; set; }
        public string OrderStatusName { get; set; }
        public decimal Total { get; set; }
    }
}
