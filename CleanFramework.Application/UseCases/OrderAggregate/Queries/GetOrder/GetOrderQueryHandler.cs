﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanFramework.Domain.Aggregates.OrderAggregate;
using CleanFramework.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrder
{
    public class GetOrderQueryHandler : IRequestHandler<GetOrderQuery, OrderDetailModel>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public GetOrderQueryHandler(IOrderRepository orderRepository,
            IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<OrderDetailModel> Handle(GetOrderQuery request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.NoTrackedQuery
                .Where(o => o.Id == request.OrderId)
                .ProjectTo<OrderDetailModel>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            if (order == null) throw new NotFoundException(request.OrderId);

            return order;
        }
    }
}
