﻿using System;
using System.Collections.Generic;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrder
{
    public class OrderDetailModel
    {
        public int Id { get; set; }
        public DateTimeOffset OrderDate { get; set; }
        public string OrderStatusName { get; set; }
        public string Description { get; set; }
        public decimal Total { get; set; }
        public AddressModel Address { get; set; }
        public List<OrderItemModel> OrderItems { get; set; }
        public BuyerModel Buyer { get; set; }
        public PaymentMethodModel PaymentMethod { get; set; }
    }

    public class AddressModel
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string Country { get; set; }
    }

    public class OrderItemModel
    {
        public string ProductName { get; set; }
        public int Units { get; set; }
        public decimal UnitPrice { get; set; }
        public string PictureUrl { get; set; }
    }

    public class BuyerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class PaymentMethodModel
    {
        public int Id { get; set; }
        public CardTypeModel CardType { get; set; }
    }

    public class CardTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
