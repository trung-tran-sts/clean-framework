﻿using AutoMapper;
using CleanFramework.Domain.Aggregates.BuyerAggregate;
using CleanFramework.Domain.Aggregates.OrderAggregate;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrder
{
    public class GetOrderProfile : Profile
    {
        public GetOrderProfile()
        {
            CreateMap<Order, OrderDetailModel>()
                .ForMember(o => o.Total, opt => opt.MapFrom(Order.GetTotalExpr));
            CreateMap<Address, AddressModel>();
            CreateMap<OrderItem, OrderItemModel>();
            CreateMap<Buyer, BuyerModel>();
            CreateMap<CardType, CardTypeModel>();
            CreateMap<PaymentMethod, PaymentMethodModel>();
        }
    }
}
