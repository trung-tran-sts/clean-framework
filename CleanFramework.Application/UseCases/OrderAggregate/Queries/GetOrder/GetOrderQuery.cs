﻿using MediatR;

namespace CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrder
{
    public class GetOrderQuery : IRequest<OrderDetailModel>
    {
        public GetOrderQuery(int orderId)
        {
            OrderId = orderId;
        }

        public int OrderId { get; }
    }
}
