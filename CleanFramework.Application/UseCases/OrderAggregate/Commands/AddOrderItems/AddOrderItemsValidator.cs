﻿using FluentValidation;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.AddOrderItems
{
    public class AddOrderItemsValidator : AbstractValidator<AddOrderItemsCommand>
    {
        public AddOrderItemsValidator()
        {
            RuleFor(o => o.OrderId).GreaterThan(0);
            RuleFor(o => o.OrderItems).NotEmpty();
        }
    }
}
