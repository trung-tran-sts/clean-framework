﻿using CleanFramework.BuildingBlocks.PubSub;
using CleanFramework.Domain.Aggregates.OrderAggregate;
using CleanFramework.Domain.Exceptions;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.AddOrderItems
{
    public class AddOrderItemsCommandHandler : IRequestHandler<AddOrderItemsCommand>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMediator _mediator;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILogger<AddOrderItemsCommandHandler> _logger;

        // Using DI to inject infrastructure persistence Repositories
        public AddOrderItemsCommandHandler(IMediator mediator,
            IEventPublisher eventPublisher,
            IOrderRepository orderRepository,
            ILogger<AddOrderItemsCommandHandler> logger)
        {
            _orderRepository = orderRepository;
            _mediator = mediator;
            _eventPublisher = eventPublisher;
            _logger = logger;
        }

        public async Task<Unit> Handle(AddOrderItemsCommand request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.FindByIdAsync(request.OrderId);

            if (order == null)
            {
                throw new NotFoundException(request.OrderId);
            }

            foreach (var item in request.OrderItems)
            {
                order.AddOrderItem(item.ProductName, item.UnitPrice, item.Discount, item.PictureUrl, item.Units);
            }

            _logger.LogInformation("----- Add Order Items - Order: {@OrderId}", order.Id);

            _orderRepository.Update(order);

            var success = await _orderRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
