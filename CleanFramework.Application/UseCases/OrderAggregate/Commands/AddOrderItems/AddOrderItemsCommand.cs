﻿using MediatR;
using System.Collections.Generic;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.AddOrderItems
{
    public class AddOrderItemsCommand : IRequest
    {
        public AddOrderItemsCommand(int orderId, IEnumerable<OrderItemModel> orderItems)
        {
            OrderId = orderId;
            OrderItems = orderItems;
        }

        public int OrderId { get; }
        public IEnumerable<OrderItemModel> OrderItems { get; }
    }
}
