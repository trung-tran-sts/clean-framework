﻿using CleanFramework.BuildingBlocks.PubSub;
using CleanFramework.Domain.Aggregates.OrderAggregate;
using CleanFramework.Domain.Exceptions;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.RemoveOrderItems
{
    public class RemoveOrderItemsCommandHandler : IRequestHandler<RemoveOrderItemsCommand>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMediator _mediator;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILogger<RemoveOrderItemsCommandHandler> _logger;

        // Using DI to inject infrastructure persistence Repositories
        public RemoveOrderItemsCommandHandler(IMediator mediator,
            IEventPublisher eventPublisher,
            IOrderRepository orderRepository,
            ILogger<RemoveOrderItemsCommandHandler> logger)
        {
            _orderRepository = orderRepository;
            _mediator = mediator;
            _eventPublisher = eventPublisher;
            _logger = logger;
        }

        public async Task<Unit> Handle(RemoveOrderItemsCommand request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.FindByIdAsync(request.OrderId);

            if (order == null)
            {
                throw new NotFoundException(request.OrderId);
            }

            foreach (var productName in request.ProductNames)
            {
                order.RemoveOrderItem(productName);
            }

            _logger.LogInformation("----- Remove Order Items - Order: {@OrderId}", order.Id);

            _orderRepository.Update(order);

            var success = await _orderRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
