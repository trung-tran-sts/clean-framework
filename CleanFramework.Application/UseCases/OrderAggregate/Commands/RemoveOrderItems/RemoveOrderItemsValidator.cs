﻿using FluentValidation;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.RemoveOrderItems
{
    public class RemoveOrderItemsValidator : AbstractValidator<RemoveOrderItemsCommand>
    {
        public RemoveOrderItemsValidator()
        {
            RuleFor(o => o.OrderId).GreaterThan(0);
            RuleFor(o => o.ProductNames).NotEmpty();
        }
    }
}
