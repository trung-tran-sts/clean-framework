﻿using MediatR;
using System.Collections.Generic;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.RemoveOrderItems
{
    public class RemoveOrderItemsCommand : IRequest
    {
        public RemoveOrderItemsCommand(int orderId, IEnumerable<string> productNames)
        {
            OrderId = orderId;
            ProductNames = productNames;
        }

        public int OrderId { get; }
        public IEnumerable<string> ProductNames { get; }
    }
}
