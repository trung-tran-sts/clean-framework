﻿namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.CreateOrder
{
    public class OrderItemModel
    {
        public OrderItemModel(string productName, decimal unitPrice,
            decimal discount, int units, string pictureUrl)
        {
            ProductName = productName;
            UnitPrice = unitPrice;
            Discount = discount;
            Units = units;
            PictureUrl = pictureUrl;
        }

        public string ProductName { get; }
        public decimal UnitPrice { get; }
        public decimal Discount { get; }
        public int Units { get; }
        public string PictureUrl { get; }
    }
}
