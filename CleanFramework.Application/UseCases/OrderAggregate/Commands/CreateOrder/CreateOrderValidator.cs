﻿using FluentValidation;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.CreateOrder
{
    public class CreateOrderValidator : AbstractValidator<CreateOrderCommand>
    {
        public CreateOrderValidator()
        {
            RuleFor(o => o.CardNumber).NotEmpty();
            RuleFor(o => o.OrderItems).NotEmpty();
        }
    }
}
