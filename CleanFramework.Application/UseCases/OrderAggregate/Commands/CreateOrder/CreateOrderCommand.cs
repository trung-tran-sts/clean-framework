﻿using CleanFramework.Application.Base;
using System;
using System.Collections.Generic;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.CreateOrder
{
    public class CreateOrderCommand : ITransactionalCommand<int>
    {
        public CreateOrderCommand(int? buyerId, string buyerName, string city,
            string street, string state, string country, string zipCode,
            string cardNumber, string cardHolderName, DateTimeOffset cardExpiration,
            string cardSecurityNumber, int cardTypeId, IEnumerable<OrderItemModel> orderItems)
        {
            BuyerId = buyerId;
            BuyerName = buyerName;
            City = city;
            Street = street;
            State = state;
            Country = country;
            ZipCode = zipCode;
            CardNumber = cardNumber;
            CardHolderName = cardHolderName;
            CardExpiration = cardExpiration;
            CardSecurityNumber = cardSecurityNumber;
            CardTypeId = cardTypeId;
            OrderItems = orderItems;
        }

        public int? BuyerId { get; }
        public string BuyerName { get; }
        public string City { get; }
        public string Street { get; }
        public string State { get; }
        public string Country { get; }
        public string ZipCode { get; }
        public string CardNumber { get; }
        public string CardHolderName { get; }
        public DateTimeOffset CardExpiration { get; }
        public string CardSecurityNumber { get; }
        public int CardTypeId { get; }
        public IEnumerable<OrderItemModel> OrderItems { get; }
    }
}
