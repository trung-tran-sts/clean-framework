﻿using CleanFramework.Application.IntegrationEvents.OrderStarted;
using CleanFramework.BuildingBlocks.PubSub;
using CleanFramework.Domain.Aggregates.OrderAggregate;
using CleanFramework.Domain.Aggregates.OrderAggregate.Factories;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.UseCases.OrderAggregate.Commands.CreateOrder
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, int>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMediator _mediator;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILogger<CreateOrderCommandHandler> _logger;
        private readonly IOrderFactory _orderFactory;

        // Using DI to inject infrastructure persistence Repositories
        public CreateOrderCommandHandler(IMediator mediator,
            IOrderFactory orderFactory,
            IEventPublisher eventPublisher,
            IOrderRepository orderRepository,
            ILogger<CreateOrderCommandHandler> logger)
        {
            _orderRepository = orderRepository;
            _mediator = mediator;
            _orderFactory = orderFactory;
            _eventPublisher = eventPublisher;
            _logger = logger;
        }

        public async Task<int> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            var orderStartedIntegrationEvent = new OrderStartedIntegrationEvent(
                request.BuyerId,
                request.BuyerName);
            await _eventPublisher.PublishAsync(
                nameof(OrderStartedIntegrationEvent), null, orderStartedIntegrationEvent);

            var address = new Address(request.Street, request.City, request.State, request.Country, request.ZipCode);
            var order = _orderFactory.CreateNewOrder(address, request.CardTypeId, request.CardNumber, request.CardSecurityNumber,
                request.CardHolderName, request.CardExpiration,
                request.BuyerName, request.BuyerId);

            foreach (var item in request.OrderItems)
            {
                order.AddOrderItem(item.ProductName, item.UnitPrice, item.Discount, item.PictureUrl, item.Units);
            }

            _logger.LogInformation("----- Creating Order - Order: {@Order}", order);

            _orderRepository.Add(order);

            var success = await _orderRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);

            return order.Id;
        }
    }
}
