﻿using AutoMapper;
using CleanFramework.Application.Common.Services;
using CleanFramework.Application.Constants;
using CleanFramework.Domain.Aggregates.BuyerAggregate;
using Dapper;
using MediatR;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetBuyers
{
    /// <summary>
    /// [Important] 
    /// This query uses Dapper for demo purpose
    /// Change to other query handlers to see sample for EF Core
    /// </summary>
    public class GetBuyersQueryHandler : IRequestHandler<GetBuyersQuery, IEnumerable<BuyerModel>>
    {
        private readonly IBuyerRepository _buyerRepository;
        private readonly IMapper _mapper;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public GetBuyersQueryHandler(IMapper mapper,
            IBuyerRepository buyerRepository,
            ISqlConnectionProvider sqlConnectionProvider)
        {
            _mapper = mapper;
            _buyerRepository = buyerRepository;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<IEnumerable<BuyerModel>> Handle(GetBuyersQuery request, CancellationToken cancellationToken)
        {
            var columns = new
            {
                Buyer = new
                {
                    Id = nameof(Buyer.Id),
                    Name = nameof(Buyer.Name)
                }
            };
            var rawQuery = new StringBuilder($"SELECT " +
                $"buyer.{columns.Buyer.Id}," +
                $"buyer.{columns.Buyer.Name}\n" +
                $"FROM {nameof(Buyer)} buyer");

            var supportedJoins = new { };
            var joins = new HashSet<string>();
            var wheres = new List<string>();
            var orderBys = new List<string>();
            var takeClause = string.Empty;
            var args = new Dictionary<string, object>();

            if (!string.IsNullOrWhiteSpace(request.Terms))
            {
                wheres.Add($"CAST(buyer.{columns.Buyer.Id} as varchar) = @terms OR buyer.{columns.Buyer.Name} LIKE @nameContains");
                args["terms"] = request.Terms;
                args["nameContains"] = $"%{request.Terms}%";
            }

            if (request.Take != null || !request.CanGetAll())
            {
                var take = request.Take ?? QueryConstants.DefaultTake;
                takeClause = $"FETCH NEXT @take ROWS ONLY";
                args["take"] = take;
            }

            if (joins.Count > 0)
            {
                rawQuery.Append("\n").Append(string.Join('\n', joins));
            }

            if (wheres.Count > 0)
            {
                rawQuery.Append("\n").Append("WHERE (").Append(string.Join(") AND (", wheres)).Append(")");
            }

            args["skip"] = request.Skip;
            orderBys.Add($"buyer.{columns.Buyer.Name} ASC");

            var queryStr = rawQuery
                .Append("\n").Append($"ORDER BY {string.Join(',', orderBys)}")
                .Append("\n").Append("OFFSET @skip ROWS")
                .Append("\n").Append(takeClause)
                .ToString();

            using (var connection = _sqlConnectionProvider.CreateConnection())
            {
                var buyers = (await connection.QueryAsync<BuyerModel>(queryStr, args));
                return buyers;
            }
        }
    }
}
