﻿namespace CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetBuyers
{
    public class BuyerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
