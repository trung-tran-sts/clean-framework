﻿using CleanFramework.Application.Base;
using MediatR;
using System.Collections.Generic;

namespace CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetBuyers
{
    public class GetBuyersQuery : BaseQueryRequest, IRequest<IEnumerable<BuyerModel>>
    {
        public GetBuyersQuery(string terms, int skip, int? take) : base(terms, skip, take)
        {
        }

        public override bool CanGetAll() => false;
    }
}
