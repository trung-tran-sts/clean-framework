﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanFramework.Application.Common.Services;
using CleanFramework.Domain.Aggregates.BuyerAggregate;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetCardTypes
{
    public class GetCardTypesQueryHandler : IRequestHandler<GetCardTypesQuery, IEnumerable<CardTypeModel>>
    {
        private readonly IMapper _mapper;
        private readonly IAppCache _appCache;

        public GetCardTypesQueryHandler(IMapper mapper,
            IAppCache appCache)
        {
            _mapper = mapper;
            _appCache = appCache;
        }

        public async Task<IEnumerable<CardTypeModel>> Handle(GetCardTypesQuery request, CancellationToken cancellationToken)
        {
            return await _appCache.GetOrAddAsync(nameof(GetCardTypesQuery),
                () => Task.FromResult(Domain.Base.Enumeration.GetAll<CardType>()
                    .AsQueryable()
                    .ProjectTo<CardTypeModel>(_mapper.ConfigurationProvider)
                    .ToArray()));
        }
    }
}
