﻿using AutoMapper;
using CleanFramework.Domain.Aggregates.BuyerAggregate;

namespace CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetCardTypes
{
    public class GetCardTypesProfile : Profile
    {
        public GetCardTypesProfile()
        {
            CreateMap<CardType, CardTypeModel>();
        }
    }
}
