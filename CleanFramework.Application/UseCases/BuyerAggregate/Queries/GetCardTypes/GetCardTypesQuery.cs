﻿using MediatR;
using System.Collections.Generic;

namespace CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetCardTypes
{
    public class GetCardTypesQuery : IRequest<IEnumerable<CardTypeModel>>
    {
    }
}
