﻿namespace CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetCardTypes
{
    public class CardTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
