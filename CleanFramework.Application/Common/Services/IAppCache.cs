﻿using System;
using System.Threading.Tasks;

namespace CleanFramework.Application.Common.Services
{
    public interface IAppCache
    {
        Task<bool> TrySetAsync<T>(string cacheKey, T cacheValue, TimeSpan? expiration = null);
        Task<T> GetOrAddAsync<T>(string cacheKey, Func<Task<T>> createFunc, TimeSpan? expiration = null);
        Task<T> GetAsync<T>(string cacheKey);
    }
}
