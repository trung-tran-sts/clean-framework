﻿using System.Data.SqlClient;

namespace CleanFramework.Application.Common.Services
{
    public interface ISqlConnectionProvider
    {
        SqlConnection CreateConnection();
    }
}
