﻿using System.Security.Claims;

namespace CleanFramework.Application.Common.Identity
{
    public interface ICurrentUserProvider
    {
        ClaimsPrincipal User { get; }
        int? UserId { get; }
    }

    public class NullCurrentUserProvider : ICurrentUserProvider
    {
        public ClaimsPrincipal User { get; }
        public int? UserId { get; }
    }
}
