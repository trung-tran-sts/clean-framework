﻿using CleanFramework.Common.Reflection;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.Common.Behaviors
{
    public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<LoggingBehavior<TRequest, TResponse>> _logger;
        public LoggingBehavior(ILogger<LoggingBehavior<TRequest, TResponse>> logger) => _logger = logger;

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var requestTypeName = request.GetGenericTypeName();
            _logger.LogInformation("----- Handling command {CommandName} ({@Command})", requestTypeName, request);
            var response = await next();
            _logger.LogInformation("----- Command {CommandName} handled - response: {@Response}", requestTypeName, response);

            return response;
        }
    }
}
