﻿using MediatR;

namespace CleanFramework.Application.IntegrationEvents.SwitchboardOrderStarted
{
    public class SwitchboardOrderStartedEvent : INotification
    {
        public int BuyerId { get; set; }
        public string Note { get; set; }
    }
}
