﻿using CleanFramework.Domain.Aggregates.BuyerAggregate;
using CleanFramework.Domain.Exceptions;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Application.IntegrationEvents.SwitchboardOrderStarted
{
    public class SwitchboardOrderStartedEventHandler : INotificationHandler<SwitchboardOrderStartedEvent>
    {
        private readonly IBuyerRepository _buyerReadonlyRepository;
        private readonly ILogger<SwitchboardOrderStartedEventHandler> _logger;

        public SwitchboardOrderStartedEventHandler(IBuyerRepository buyerReadonlyRepository,
            ILogger<SwitchboardOrderStartedEventHandler> logger)
        {
            _buyerReadonlyRepository = buyerReadonlyRepository;
            _logger = logger;
        }

        public async Task Handle(SwitchboardOrderStartedEvent notification, CancellationToken cancellationToken)
        {
            var buyer = await _buyerReadonlyRepository.FindByIdAsync(notification.BuyerId);

            if (buyer == null)
            {
                throw new NotFoundException(notification.BuyerId);
            }

            _logger.LogInformation("Received order from switchboard:\n" +
                "Buyer: {BuyerName}\n" +
                "Note: {Note}", buyer.Name, notification.Note);
        }
    }
}
