﻿namespace CleanFramework.Application.IntegrationEvents.OrderStarted
{
    public class OrderStartedIntegrationEvent
    {
        public int? BuyerId { get; }
        public string BuyerName { get; }

        public OrderStartedIntegrationEvent(int? buyerId, string buyerName)
        {
            BuyerId = buyerId;
            BuyerName = buyerName;
        }
    }
}
