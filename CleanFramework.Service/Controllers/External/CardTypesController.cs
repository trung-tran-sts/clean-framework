﻿using CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetCardTypes;
using CleanFramework.Service.Configuration;
using CleanFramework.Service.Controllers.BuyerAggregate;
using CleanFramework.Service.Infrastructure.Auth.Schemes.Client;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Endpoints = CleanFramework.Service.Configuration.ApiRoutes.External.CardType;

namespace CleanFramework.Service.Controllers.External
{
    [Route(Endpoints.Root)]
    [ApiController]
    [Authorize(AuthenticationSchemes = ClientAuthenticationDefaults.AuthenticationScheme)]
    public class ExternalCardTypesController : BaseApiController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<CardTypesController> _logger;

        public ExternalCardTypesController(IMediator mediator,
            ILogger<CardTypesController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet(Endpoints.GetCardTypes)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(IEnumerable<CardTypeModel>))]
        [SwaggerOperation(Tags = new[] { ApiRoutes.ExternalGroupName })]
        public async Task<IActionResult> GetCardTypesAsync()
        {
            var cardTypes = await _mediator.Send(new GetCardTypesQuery());

            return Ok(cardTypes);
        }
    }
}
