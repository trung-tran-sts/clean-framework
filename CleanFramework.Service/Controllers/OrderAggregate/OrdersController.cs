﻿using CleanFramework.Application.UseCases.OrderAggregate.Commands.AddOrderItems;
using CleanFramework.Application.UseCases.OrderAggregate.Commands.CreateOrder;
using CleanFramework.Application.UseCases.OrderAggregate.Commands.RemoveOrderItems;
using CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrder;
using CleanFramework.Application.UseCases.OrderAggregate.Queries.GetOrders;
using CleanFramework.Common.Reflection;
using CleanFramework.Service.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Endpoints = CleanFramework.Service.Configuration.ApiRoutes.Order;

namespace CleanFramework.Service.Controllers.OrderAggregate
{
    [Route(Endpoints.Root)]
    [ApiController]
    public class OrdersController : BaseApiController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(IMediator mediator,
            ILogger<OrdersController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet(Endpoints.GetOrder)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(OrderDetailModel))]
        public async Task<IActionResult> GetOrderAsync(int orderId)
        {
            var order = await _mediator.Send(new GetOrderQuery(orderId));

            return Ok(order);
        }

        [HttpGet(Endpoints.GetOrders)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(IEnumerable<OrderSummaryModel>))]
        public async Task<IActionResult> GetOrdersAsync([FromQuery] GetOrdersQueryModel model)
        {
            var query = new GetOrdersQuery(model.Terms, model.Skip, model.Take,
                model.FromDate, model.ToDate, model.Country, model.Status);

            var orders = await _mediator.Send(query);

            return Ok(orders);
        }

        [HttpPost(Endpoints.CreateOrder)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(int))]
        public async Task<IActionResult> CreateOrderAsync([FromBody] CreateOrderCommand createOrderCmd)
        {
            _logger.LogInformation(
                "----- Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                createOrderCmd.GetGenericTypeName(),
                nameof(createOrderCmd.BuyerId),
                createOrderCmd.BuyerId,
                createOrderCmd);

            var createdOrderId = await _mediator.Send(createOrderCmd);

            return Ok(createdOrderId);
        }

        [HttpPost(Endpoints.AddOrderItems)]
        [SwaggerResponse((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> AddOrderItemsAsync([FromBody] AddOrderItemsCommand cmd)
        {
            await _mediator.Send(cmd);

            return NoContent();
        }

        [HttpDelete(Endpoints.RemoveOrderItems)]
        [SwaggerResponse((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> RemoveOrderItemsAsync([FromBody] RemoveOrderItemsCommand cmd)
        {
            await _mediator.Send(cmd);

            return NoContent();
        }

        #region View models
        // [Important] some FromQuery complex models must have parameterless constructor
        // --> can not encapsulate data --> need to map from a view models
        public class GetOrdersQueryModel : BaseQueryRequestModel
        {
            public DateTimeOffset? FromDate { get; set; }
            public DateTimeOffset? ToDate { get; set; }
            public string Country { get; set; }
            public int? Status { get; set; }
        }
        #endregion
    }
}
