﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using Endpoints = CleanFramework.Service.Configuration.ApiRoutes.Admin;
using Policies = CleanFramework.Service.Infrastructure.Auth.Policies.ServicePolicy.Admin;

namespace CleanFramework.Service.Controllers.Admin
{
    [Route(Endpoints.Root)]
    [ApiController]
    [Authorize]
    public class AdminController : BaseApiController
    {
        private readonly ILogger<AdminController> _logger;

        public AdminController(ILogger<AdminController> logger)
        {
            _logger = logger;
        }

        [HttpPost(Endpoints.DoTaskA)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(string))]
        [Authorize(Policies.CanDoTaskA)]
        public IActionResult DoTaskA()
        {
            return Ok("Finished task A!");
        }

        [HttpPost(Endpoints.DoTaskB)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(string))]
        [Authorize(Policies.CanDoTaskB)]
        public IActionResult DoTaskB()
        {
            return Ok("Finished task B!");
        }
    }
}
