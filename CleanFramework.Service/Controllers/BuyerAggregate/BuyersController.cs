﻿using CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetBuyers;
using CleanFramework.Service.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Endpoints = CleanFramework.Service.Configuration.ApiRoutes.Buyer;

namespace CleanFramework.Service.Controllers.BuyerAggregate
{
    [Route(Endpoints.Root)]
    [ApiController]
    public class BuyersController : BaseApiController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<BuyersController> _logger;

        public BuyersController(IMediator mediator,
            ILogger<BuyersController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet(Endpoints.GetBuyers)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(IEnumerable<BuyerModel>))]
        public async Task<IActionResult> GetBuyersAsync([FromQuery] GetBuyersQueryModel model)
        {
            var query = new GetBuyersQuery(model.Terms, model.Skip, model.Take);

            var buyers = await _mediator.Send(query);

            return Ok(buyers);
        }

        #region View models
        // [Important] some FromQuery complex models must have parameterless constructor
        // --> can not encapsulate data --> need to map from a view models
        public class GetBuyersQueryModel : BaseQueryRequestModel
        {
        }
        #endregion
    }
}
