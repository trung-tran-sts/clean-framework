﻿using CleanFramework.Application.UseCases.BuyerAggregate.Queries.GetCardTypes;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Endpoints = CleanFramework.Service.Configuration.ApiRoutes.CardType;

namespace CleanFramework.Service.Controllers.BuyerAggregate
{
    [Route(Endpoints.Root)]
    [ApiController]
    public class CardTypesController : BaseApiController
    {

        private readonly IMediator _mediator;
        private readonly ILogger<CardTypesController> _logger;

        public CardTypesController(IMediator mediator,
            ILogger<CardTypesController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet(Endpoints.GetCardTypes)]
        [SwaggerResponse((int)HttpStatusCode.OK, null, typeof(IEnumerable<CardTypeModel>))]
        public async Task<IActionResult> GetCardTypesAsync()
        {
            var cardTypes = await _mediator.Send(new GetCardTypesQuery());

            return Ok(cardTypes);
        }
    }
}
