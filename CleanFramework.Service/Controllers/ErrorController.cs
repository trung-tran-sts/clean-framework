﻿using CleanFramework.Domain.Exceptions;
using CleanFramework.Service.Configuration;
using CleanFramework.Service.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Net;

namespace CleanFramework.Service.Controllers
{
    // [Important] used to handle global exceptions
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route(ApiRoutes.Error.Root)]
    public class ErrorController : BaseApiController
    {
        private readonly IWebHostEnvironment _env;
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(IWebHostEnvironment env,
            ILogger<ErrorController> logger)
        {
            _env = env;
            _logger = logger;
        }

        [Route(ApiRoutes.Error.HandleException)]
        public IActionResult HandleException()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context.Error;

            if (exception == null) return BadRequest();

            ApiResponse response = null;

            if (exception is NotFoundException notFoundEx)
            {
                response = ApiResponse.NotFound(notFoundEx.DataObject, notFoundEx.Message);
                return NotFound(response);
            }
            else if (exception is BaseException baseEx)
            {
                response = ApiResponse.Exception(baseEx);
                return BadRequest(response);
            }

            if (response == null)
            {
                if (_env.IsDevelopment())
                    response = ApiResponse.UnknownError(exception, exception.Message);
                else response = ApiResponse.UnknownError();
            }

            _logger.LogError(
                $"{(int)HttpStatusCode.InternalServerError} {nameof(HttpStatusCode.InternalServerError)}");

            return Error(response);
        }
    }
}
