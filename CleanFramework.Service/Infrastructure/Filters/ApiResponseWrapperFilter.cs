﻿using CleanFramework.Common.Reflection;
using CleanFramework.Service.Attributes;
using CleanFramework.Service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace CleanFramework.Service.Infrastructure.Filters
{
    public class ApiResponseWrapperFilter : ResultFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            var controllerAction = context.ActionDescriptor as ControllerActionDescriptor;
            if (controllerAction == null)
            {
                return;
            }

            var objectResult = context.Result as ObjectResult;
            if (objectResult == null)
            {
                return;
            }

            var disableWrap = ReflectionHelper.GetAttributesOfMemberOrType<DisableWrapResult>(
                controllerAction.MethodInfo).Any();

            if (disableWrap)
            {
                return;
            }

            if (objectResult.Value is ApiResponse == false)
            {
                objectResult.Value = ApiResponse.Object(objectResult.Value);
            }
        }
    }
}
