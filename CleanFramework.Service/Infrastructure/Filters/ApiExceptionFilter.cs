﻿using CleanFramework.Common.Streaming;
using CleanFramework.Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Service.Infrastructure.Filters
{
    // [Important] used to handle predictable exceptions, per action/controller
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<ApiExceptionFilter> _logger;

        public ApiExceptionFilter(ILogger<ApiExceptionFilter> logger)
        {
            _logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            LogErrorRequestAsync(context.Exception, context.HttpContext).Wait();
        }

        public override async Task OnExceptionAsync(ExceptionContext context)
        {
            await LogErrorRequestAsync(context.Exception, context.HttpContext);
        }

        private async Task LogErrorRequestAsync(Exception ex, HttpContext httpContext)
        {
            var acceptedLevel = new[] { LogLevel.Error, LogLevel.Critical };
            if (ex is BaseException baseEx && !acceptedLevel.Contains(baseEx.LogLevel))
            {
                return;
            }

            var request = httpContext.Request;
            var bodyInfo = string.Empty;

            if (request.ContentLength > 0 &&
                request.ContentLength <= Startup.SerilogSettings.MaxBodyLengthForLogging)
            {
                request.Body.Position = 0;
                var bodyRaw = await request.Body.ReadAsStringAsync();
                bodyInfo = string.IsNullOrEmpty(bodyRaw) ? "" : $"\r\n---- Raw body ----\r\n{bodyRaw}\r\n";
            }

            Dictionary<string, StringValues> form = null;
            if (request.HasFormContentType)
                form = new Dictionary<string, StringValues>(request.Form);

            _logger.LogError("Exception on Request: {@Request}{Body}", new
            {
                request.ContentLength,
                request.ContentType,
                Host = request.Host.Value,
                request.IsHttps,
                request.Method,
                request.Path,
                request.Protocol,
                QueryString = request.QueryString.Value,
                request.Scheme,
                Form = form
            }, bodyInfo);
        }
    }
}
