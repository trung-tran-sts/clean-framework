﻿using CleanFramework.Service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace CleanFramework.Service.Infrastructure.Filters
{
    public class ValidateModelStateFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid)
            {
                return;
            }

            var validationErrors = context.ModelState.Values
                .SelectMany(o => o.Errors)
                .Select(e => e.ErrorMessage)
                .ToArray();

            var apiResponse = ApiResponse.BadRequest(validationErrors);

            context.Result = new BadRequestObjectResult(apiResponse);
        }
    }
}
