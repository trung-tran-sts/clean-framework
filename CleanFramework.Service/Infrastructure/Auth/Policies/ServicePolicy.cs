﻿using CleanFramework.Application.Auth.Policies;
using CleanFramework.Domain.Constants;
using System.Collections.Generic;

namespace CleanFramework.Service.Infrastructure.Auth.Policies
{
    public static class ServicePolicy
    {
        public static class Admin
        {
            const string Prefix = nameof(Admin) + ":";
            public const string CanDoTaskA = Prefix + nameof(CanDoTaskA);
            public const string CanDoTaskB = Prefix + nameof(CanDoTaskB);
        }

        public static IDictionary<string, string> Entries = new Dictionary<string, string>()
        {
            [Admin.CanDoTaskA] =
                $"{ApplicationPolicy.HasRole}:{RoleEnum.Administrator};" +
                $"{ApplicationPolicy.HasUserName}:trung.tran;",

            [Admin.CanDoTaskB] =
                $"{ApplicationPolicy.IsSpecialUser};"
        };

        // [Important] merge with ApplicationPolicy
        static ServicePolicy()
        {
            foreach (var applicationEntry in ApplicationPolicy.Entries)
            {
                Entries.Add(applicationEntry);
            }
        }
    }
}
