﻿using CleanFramework.Service.Configuration.Client;
using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;

namespace CleanFramework.Service.Infrastructure.Auth.Schemes.Client
{
    public class ClientAuthenticationOptions : AuthenticationSchemeOptions
    {
        public IEnumerable<ApplicationClient> Clients { get; set; }
    }
}
