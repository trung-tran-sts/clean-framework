﻿namespace CleanFramework.Service.Infrastructure.Auth.Schemes.Client
{
    public static class ClientAuthenticationDefaults
    {
        public const string AuthenticationScheme = "Basic";
    }
}
