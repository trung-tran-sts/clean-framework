﻿using CleanFramework.Application.Common.Identity;
using CleanFramework.Common.DependencyInjection;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace CleanFramework.Service.Infrastructure.Auth
{
    public class CurrentUserProvider : ICurrentUserProvider, IScopedService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CurrentUserProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public ClaimsPrincipal User => _httpContextAccessor.HttpContext?.User;
        public int? UserId => int.TryParse(User?.Identity?.Name, out int userId) ? userId : default(int?);
    }
}
