﻿using CleanFramework.Application.Auth.Policies;
using CleanFramework.Application.Auth.Policies.HasUserName;
using CleanFramework.Service.Infrastructure.Auth.Policies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Service.Infrastructure.Auth
{
    // [Important] demo only: process policy names by syntax: policyA[_args];policyB[_args]...
    // So we don't have to create specific policy for each endpoint, just use the syntax with generic policies defined
    // Or we can change the requirements based on configuration in database, etc.
    public class ApplicationPolicyProvider : IAuthorizationPolicyProvider
    {
        const char EntrySplit = ';';
        const char PartSplit = ':';
        const int PolicyNameIdx = 0;

        private readonly DefaultAuthorizationPolicyProvider _backupPolicyProvider;

        public ApplicationPolicyProvider(IOptions<AuthorizationOptions> options)
        {
            // ASP.NET Core only uses one authorization policy provider, so if the custom implementation
            // doesn't handle all policies it should fall back to an alternate provider.
            _backupPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        }

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync() => _backupPolicyProvider.GetDefaultPolicyAsync();

        public Task<AuthorizationPolicy> GetFallbackPolicyAsync() => _backupPolicyProvider.GetFallbackPolicyAsync();

        public async Task<AuthorizationPolicy> GetPolicyAsync(string policyId)
        {
            // [Important] demo only: access configuration database
            if (!ServicePolicy.Entries.TryGetValue(policyId, out var policyConfig))
            {
                var policy = await _backupPolicyProvider.GetPolicyAsync(policyId);
                return policy;
            }

            var policyEntries = policyConfig.Split(EntrySplit, StringSplitOptions.RemoveEmptyEntries);

            var policyBuilder = new AuthorizationPolicyBuilder();

            foreach (var policyEntry in policyEntries)
            {
                var policyParts = policyEntry.Split(PartSplit, StringSplitOptions.RemoveEmptyEntries);
                var policyName = policyParts[PolicyNameIdx];

                if (ServicePolicy.Entries.ContainsKey(policyName))
                {
                    var policy = await GetPolicyAsync(policyName);
                    policyBuilder = policyBuilder.Combine(policy);
                    continue;
                }

                switch (policyName)
                {
                    case ApplicationPolicy.HasUserName:
                        {
                            const int ParamLength = 2;
                            const int NameParamIdx = 1;
                            if (policyParts.Length != ParamLength) throw new ArgumentException();
                            policyBuilder.AddRequirements(new HasUserNameRequirement(policyParts[NameParamIdx]));
                            break;
                        }
                    case ApplicationPolicy.HasRole:
                        {
                            const int MinParamLength = 2;
                            const int ParamFromIdx = 1;
                            if (policyParts.Length < MinParamLength) throw new ArgumentException();
                            policyBuilder.RequireRole(policyParts.Skip(ParamFromIdx));
                            break;
                        }
                    default:
                        {
                            var policy = await _backupPolicyProvider.GetPolicyAsync(policyName);
                            policyBuilder = policyBuilder.Combine(policy);
                            break;
                        }
                }
            }

            return policyBuilder.Build();
        }
    }
}
