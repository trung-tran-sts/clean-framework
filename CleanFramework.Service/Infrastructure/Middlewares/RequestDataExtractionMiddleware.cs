﻿using CleanFramework.Application.Common.Identity;
using CleanFramework.Common.DependencyInjection;
using CleanFramework.Domain.Constants;
using Microsoft.AspNetCore.Http;
using Serilog.Context;
using System.Threading.Tasks;

namespace CleanFramework.Service.Infrastructure.Middlewares
{
    public class RequestDataExtractionMiddleware : IMiddleware, IScopedService
    {
        private readonly ICurrentUserProvider _currentUserProvider;

        public RequestDataExtractionMiddleware(ICurrentUserProvider currentUserProvider)
        {
            _currentUserProvider = currentUserProvider;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            if (_currentUserProvider.UserId == null)
            {
                // [Important] LogContext for each request
                LogContext.PushProperty(LoggingProperties.UserId, _currentUserProvider.UserId);
            }

            await next(context);
        }
    }
}
