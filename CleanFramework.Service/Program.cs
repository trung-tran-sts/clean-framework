using CleanFramework.Application.IntegrationEvents.SwitchboardOrderStarted;
using CleanFramework.Application.Jobs;
using CleanFramework.Application.Jobs.HelloWorld;
using CleanFramework.BuildingBlocks.PubSub;
using CleanFramework.Infrastructure.Logging;
using CleanFramework.Persistence;
using CleanFramework.Service.Constants;
using CleanFramework.Service.EventSubscriptions.SwitchboardOrderStarted;
using Hangfire;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;
using Serilog.Configuration;
using Serilog.Events;
using Serilog.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CleanFramework.Service
{
    public class Program
    {
        const string SupportedLoggers = nameof(SupportedLoggers);
        const string FileLoggerSectionName = nameof(Serilog) + ":" + SupportedLoggers + ":FileLogger";
        const string ConsoleLoggerSectionName = nameof(Serilog) + ":" + SupportedLoggers + ":ConsoleLogger";

        public static async Task<int> Main(string[] args)
        {
            using var seriLogger = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.WithUtcTimestamp()
                .WriteTo.HostLevelLog()
                .CreateLogger();
            var logger = new SerilogLoggerFactory(seriLogger).CreateLogger(nameof(Program));

            try
            {
                logger.LogInformation("Starting web host");

                var host = CreateHostBuilder(args).Build();

                await InitAsync(host);

                RunJobs(host);

                StartConsumers(host);

                host.Run();

                logger.LogInformation("Shutdown web host");
                return 0;
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Host terminated unexpectedly");
                return 1;
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog((context, services, configuration) =>
                {
                    configuration
                        .ReadFrom.Configuration(context.Configuration, sectionName: nameof(Serilog))
                        .ReadFrom.Services(services)
                        .WriteTo.Logger(fileConfig =>
                            fileConfig.ReadFrom.Configuration(
                                context.Configuration,
                                sectionName: FileLoggerSectionName)
                            .Filter.WithFileLoggerFilter());

                    if (context.HostingEnvironment.IsDevelopment())
                    {
                        configuration = configuration.WriteTo.Logger(consoleConfig =>
                            consoleConfig.ReadFrom.Configuration(
                                context.Configuration,
                                sectionName: ConsoleLoggerSectionName));
                    }
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        public static async Task InitAsync(IHost host)
        {
            using var serviceScope = host.Services.CreateScope();
            var orderingContext = serviceScope.ServiceProvider.GetRequiredService<OrderingContext>();
            await orderingContext.Database.MigrateAsync();
        }

        public static void RunJobs(IHost host)
        {
            var jobs = Startup.HangfireSettings.Jobs;

            if (jobs?.Any() != true) return;

            using var serviceScope = host.Services.CreateScope();
            var recurringJobManager = serviceScope.ServiceProvider.GetRequiredService<IRecurringJobManager>();

            foreach (var job in Startup.HangfireSettings.Jobs)
            {
                var count = 1;
                foreach (var cronExpr in job.CronExpressions)
                {
                    switch (job.Name)
                    {
                        case ApplicationJobNames.HelloWorldJob:
                            {
                                var serializedData = JsonConvert.SerializeObject(job.JobData);
                                var jobData = JsonConvert.DeserializeObject<HelloWorldJobData>(serializedData);

                                recurringJobManager.AddOrUpdate<IHelloWorldJob>(ApplicationJobNames.HelloWorldJob + (count++),
                                    service => service.SayHelloWorldAsync(jobData),
                                    cronExpr);
                                break;
                            }
                    }
                }
            }
        }

        public static void StartConsumers(IHost host)
        {
            // [Important] use root scope provider to create scope in each Subscription consumption
            var eventSubscriber = host.Services.GetRequiredService<IEventSubscriber>();

            var switchboardOrderStartedSubscription = eventSubscriber
                .Subscribe<SwitchboardOrderStartedSubscription>(nameof(SwitchboardOrderStartedEvent));
        }
    }

    static class ProgramExtensions
    {
        public const string HostLevelLogFile = "logs/host/host.txt";
        public const string HostLevelLogTemplate = "[{UtcTimestamp} {Level:u3}] {Message:lj}{NewLine}{Exception}";

        public static LoggerConfiguration HostLevelLog(this LoggerSinkConfiguration writeTo)
        {
            var template = HostLevelLogTemplate;
            var env = Environment.GetEnvironmentVariable(EnvironmentVariables.AspNetCoreEnv.Key);

            if (env == EnvironmentVariables.AspNetCoreEnv.Development)
            {
                return writeTo.Console(restrictedToMinimumLevel: LogEventLevel.Information, outputTemplate: template);
            }

            return writeTo.File(HostLevelLogFile,
                rollingInterval: RollingInterval.Month,
                restrictedToMinimumLevel: LogEventLevel.Information, outputTemplate: template);
        }
    }
}
