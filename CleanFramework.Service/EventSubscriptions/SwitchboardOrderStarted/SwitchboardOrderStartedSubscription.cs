﻿using CleanFramework.Application.IntegrationEvents.SwitchboardOrderStarted;
using CleanFramework.BuildingBlocks.PubSub.ApacheKafka;
using CleanFramework.Common.DependencyInjection;
using CleanFramework.Domain.Exceptions;
using Confluent.Kafka;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Contrib.WaitAndRetry;
using Polly.Retry;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CleanFramework.Service.EventSubscriptions.SwitchboardOrderStarted
{
    public class SwitchboardOrderStartedSubscription :
        ApacheKafkaSubscription<SwitchboardOrderStartedEvent>,
        ISingletonService
    {
        static readonly TimeSpan DefaultBreakTime = TimeSpan.FromSeconds(7);

        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<SwitchboardOrderStartedSubscription> _logger;

        public SwitchboardOrderStartedSubscription(IOptions<ConsumerConfig> consumerConfig,
            IServiceProvider serviceProvider,
            ILogger<SwitchboardOrderStartedSubscription> logger) : base(consumerConfig)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        // [TODO] Implement resilience
        protected override async Task DoWorkAsync(CancellationToken cancellationToken)
        {
            try
            {
                using var consumer = CreateConsumer();
                consumer.Subscribe(ConsumingTopic);

                while (!cancellationToken.IsCancellationRequested)
                {
                    ConsumeResult<string, SwitchboardOrderStartedEvent> record = null;

                    try
                    {
                        using var scope = _serviceProvider.CreateScope();
                        var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();

                        var delay = Backoff.DecorrelatedJitterBackoffV2(medianFirstRetryDelay: TimeSpan.FromSeconds(2), retryCount: 3);
                        AsyncRetryPolicy unexpectedExRetryPolicy = Policy
                            .Handle<Exception>(ex => ex is BaseException == false)
                            .WaitAndRetryAsync(delay, onRetry: (exception, delay, count, context) =>
                            {
                                _logger.LogError(exception, "Retry in {Subscription}, count: {Count}",
                                    nameof(SwitchboardOrderStartedSubscription),
                                    count);
                            });

                        await unexpectedExRetryPolicy.ExecuteAsync(
                            async (cancellationToken) =>
                            {
                                record = consumer.Consume();
                                var @event = record.Message.Value;
                                await mediator.Publish(@event, cancellationToken);
                                consumer.Commit(record);
                            },
                            cancellationToken);
                    }
                    catch (BaseException ex)
                    {
                        _logger.LogError(ex, ex.Message);

                        if (record != null)
                        {
                            consumer.Commit(record);
                        }
                    }
                    catch (TaskCanceledException)
                    {
                        _logger.LogError("Task was cancelled in {Subscription}",
                            nameof(SwitchboardOrderStartedSubscription));
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Failed to perform work in {Subscription}, retry in {BreakTime}s",
                            nameof(SwitchboardOrderStartedSubscription));

                        await Task.Delay(DefaultBreakTime);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to perform work in {Subscription}", nameof(SwitchboardOrderStartedSubscription));
                throw;
            }
        }
    }
}
