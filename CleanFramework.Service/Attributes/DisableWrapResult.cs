﻿using System;

namespace CleanFramework.Service.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Method)]
    public class DisableWrapResult : Attribute
    {
    }
}
