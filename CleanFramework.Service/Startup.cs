using CleanFramework.Common.Logging.Extensions;
using CleanFramework.Common.Logging.Options;
using CleanFramework.Common.Web.Requests;
using CleanFramework.Service.Configuration;
using CleanFramework.Service.Configuration.App;
using CleanFramework.Service.Configuration.BackgroundJob;
using CleanFramework.Service.Configuration.Client;
using CleanFramework.Service.Configuration.Logging;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;

namespace CleanFramework.Service
{
    public class Startup
    {
        const string RequestLoggingOptionsKey = nameof(Serilog) + ":" + nameof(RequestLoggingOptions);

        private readonly List<IDisposable> _resources;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _resources = new List<IDisposable>();
            AppSettings = new AppSettings();
            SerilogSettings = new SerilogSettings();
            HangfireSettings = new HangfireSettings();
            ClientSettings = new ClientSettings();

            Configuration.Bind(nameof(AppSettings), AppSettings);
            Configuration.Bind(nameof(Serilog), SerilogSettings);
            Configuration.Bind(nameof(HangfireSettings), HangfireSettings);
            Configuration.Bind(nameof(ClientSettings), ClientSettings);
        }

        /// <summary>
        /// [Important] This also includes:
        /// 1. ChainedConfigurationProvider : Adds an existing IConfiguration as a source. In the default configuration case, adds the host configuration and setting it as the first source for the app configuration.
        /// 2. appsettings.json using the JSON configuration provider.
        /// 3. appsettings.Environment.json using the JSON configuration provider. For example, appsettings.Production.json and appsettings.Development.json
        /// 4. App secrets when the app runs in the Development environment.
        /// 5. Environment variables using the Environment Variables configuration provider.
        /// 6. Command-line arguments using the Command-line configuration provider.
        /// Ref: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.1
        /// </summary>
        public IConfiguration Configuration { get; }
        public static AppSettings AppSettings { get; private set; }
        public static SerilogSettings SerilogSettings { get; private set; }
        public static HangfireSettings HangfireSettings { get; private set; }
        public static ClientSettings ClientSettings { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabase(Configuration)
                .AddPubSubIntegration(Configuration)
                .AddHttpContextAccessor()
                .AddCaching()
                .AddApplicationLogging()
                .AddSwagger()
                .AddMediator()
                .AddResilience()
                .AddMapper()
                .AddApplicationHangfire()
                .AddApplicationServices();

            services.AddApplicationAuthentication()
                .AddApplicationAuthorization();

            services.ConfigureApiBehavior();

            services.AddApplicationControllers()
                .AddNewtonsoftJson()
                .AddValidation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            IHostApplicationLifetime applicationLifetime)
        {
            app.UseExceptionHandler($"/{ApiRoutes.Error.Root}");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
                //builder.AllowAnyOrigin();
                builder.SetIsOriginAllowed(origin =>
                {
                    return true;
                });
            });

            app.UseRequestBuffering();

            app.UseAuthentication();

            app.UseRequestDataExtraction();

            var requestLoggingOptions = Configuration.GetSection(RequestLoggingOptionsKey).Get<RequestLoggingOptions>();

            if (requestLoggingOptions != null)
            {
                if (!requestLoggingOptions.UseDefaultLogger)
                {
                    var requestLogger = Configuration.ParseLogger(
                        RequestLoggingOptionsKey, app.ApplicationServices);
                    app.UseApplicationSerilogRequestLogging(requestLoggingOptions, requestLogger);
                    _resources.Add(requestLogger);
                }
                else
                {
                    app.UseApplicationSerilogRequestLogging(requestLoggingOptions);
                }
            }

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                if (HangfireSettings.UseDashboard)
                {
                    endpoints.MapHangfireDashboard();
                }
            });

            applicationLifetime.ApplicationStarted.Register(OnApplicationStarted);
            applicationLifetime.ApplicationStopped.Register(OnApplicationStopped);
        }

        private void OnApplicationStarted()
        {
        }

        private void OnApplicationStopped()
        {
            Console.WriteLine("Cleaning resources ...");

            foreach (var resource in _resources)
                resource.Dispose();
        }
    }
}
