﻿using CleanFramework.Common.Enumerations;
using CleanFramework.Domain;
using CleanFramework.Domain.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CleanFramework.Service.Models
{
    public class ApiResponse
    {
        public ApiResponse(ResultCode code, string message = null, object data = null)
        {
            Code = code;
            _message = message;
            Data = data;
        }

        public ResultCode Code { get; }

        private string _message;
        public string Message => _message ??
            Code.GetDescription() ??
            Code.GetDisplayName() ??
            Code.GetName();

        public object Data { get; }

        [JsonExtensionData]
        public IDictionary<string, JToken> Extensions { get; set; } = new Dictionary<string, JToken>();

        public static ApiResponse Object(object data, string message = null)
            => new ApiResponse(ResultCode.Global_ObjectResult, message, data);

        public static ApiResponse BadRequest(object data = null, string message = null)
            => new ApiResponse(ResultCode.Global_BadRequest, message, data);

        public static ApiResponse Exception(BaseException exception)
            => new ApiResponse(exception.Code, exception.Message, exception.DataObject);

        public static ApiResponse NotFound(object data = null, string message = null)
            => new ApiResponse(ResultCode.Global_ResourceNotFound, message, data);

        public static ApiResponse UnknownError(object data = null, string message = null)
            => new ApiResponse(ResultCode.Global_UnknownError, message, data);
    }
}
