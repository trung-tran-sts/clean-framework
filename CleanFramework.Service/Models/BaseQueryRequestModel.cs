﻿namespace CleanFramework.Service.Models
{
    public class BaseQueryRequestModel
    {
        public string Terms { get; set; }
        public int Skip { get; set; }
        public int? Take { get; set; }
    }
}
