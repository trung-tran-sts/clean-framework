﻿namespace CleanFramework.Service.Configuration.Client
{
    public class ApplicationClient
    {
        public string Name { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
