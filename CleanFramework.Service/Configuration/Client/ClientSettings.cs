﻿using System.Collections.Generic;

namespace CleanFramework.Service.Configuration.Client
{
    public class ClientSettings
    {
        public IEnumerable<ApplicationClient> Clients { get; set; }
    }
}
