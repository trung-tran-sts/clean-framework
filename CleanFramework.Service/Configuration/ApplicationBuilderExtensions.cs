﻿using CleanFramework.Common.Logging.Options;
using CleanFramework.Service.Infrastructure.Middlewares;
using Microsoft.AspNetCore.Builder;
using Serilog;

namespace CleanFramework.Service.Configuration
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseApplicationSerilogRequestLogging(this IApplicationBuilder app,
            RequestLoggingOptions frameworkOptions, ILogger logger = null)
        {
            return app.UseSerilogRequestLogging(options =>
            {
                if (!frameworkOptions.UseDefaultLogger)
                    options.Logger = logger;

                // Customize the message template
                options.MessageTemplate = frameworkOptions.MessageTemplate;

                // Emit info-level events instead of the defaults
                if (frameworkOptions.StaticGetLevel != null)
                    options.GetLevel = (httpContext, elapsed, ex) => frameworkOptions.StaticGetLevel.Value;

                // Attach additional properties to the request completion event
                options.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    if (frameworkOptions.IncludeHost)
                        diagnosticContext.Set(nameof(httpContext.Request.Host), httpContext.Request.Host);

                    foreach (var header in frameworkOptions.EnrichHeaders)
                        diagnosticContext.Set(header.Key, httpContext.Request.Headers[header.Value]);
                };
            });
        }

        public static IApplicationBuilder UseRequestDataExtraction(this IApplicationBuilder app)
        {
            return app.UseMiddleware<RequestDataExtractionMiddleware>();
        }
    }
}
