﻿namespace CleanFramework.Service.Configuration.App
{
    public class AppSettings
    {
        public string IdentityProviderUrl { get; set; }
        public string ApplicationClientId { get; set; }
    }
}
