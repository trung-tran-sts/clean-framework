﻿using System.Collections.Generic;

namespace CleanFramework.Service.Configuration.BackgroundJob
{
    public class HangfireSettings
    {
        public string ServerName { get; set; }
        public bool UseDashboard { get; set; }
        public IEnumerable<CronJob> Jobs { get; set; }
    }
}
