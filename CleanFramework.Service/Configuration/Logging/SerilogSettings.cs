﻿namespace CleanFramework.Service.Configuration.Logging
{
    public class SerilogSettings
    {
        public long MaxBodyLengthForLogging { get; set; }
    }
}
