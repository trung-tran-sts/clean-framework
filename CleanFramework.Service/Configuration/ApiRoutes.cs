﻿namespace CleanFramework.Service.Configuration
{
    public static class ApiRoutes
    {
        public const string ExternalGroupName = "External";

        public static class Admin
        {
            public const string Root = "admin";
            public const string DoTaskA = "task-a";
            public const string DoTaskB = "task-b";
        }

        public static class Error
        {
            public const string Root = "error";
            public const string HandleException = "";
        }

        public static class CardType
        {
            public const string Root = "api/card-types";
            public const string GetCardTypes = "";
        }

        public static class Buyer
        {
            public const string Root = "api/buyers";
            public const string GetBuyers = "";
        }

        public static class Order
        {
            public const string Root = "api/orders";
            public const string GetOrder = "{orderId}";
            public const string GetOrders = "";
            public const string CreateOrder = "";
            public const string AddOrderItems = "order-items";
            public const string RemoveOrderItems = "order-items";
        }

        public static class External
        {
            public static class CardType
            {
                public const string Root = "api/external/card-types";
                public const string GetCardTypes = "";
            }
        }
    }
}
