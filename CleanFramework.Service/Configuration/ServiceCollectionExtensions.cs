﻿using CleanFramework.Application.Auth.Policies;
using CleanFramework.Application.Auth.Policies.IsSpecialUser;
using CleanFramework.Application.Common.Services;
using CleanFramework.BuildingBlocks.PubSub;
using CleanFramework.BuildingBlocks.PubSub.ApacheKafka;
using CleanFramework.Common.DependencyInjection;
using CleanFramework.Common.Validation;
using CleanFramework.Infrastructure.Caching;
using CleanFramework.Infrastructure.Resilience;
using CleanFramework.Persistence;
using CleanFramework.Service.Configuration.Constants;
using CleanFramework.Service.Infrastructure.Auth;
using CleanFramework.Service.Infrastructure.Auth.Schemes.Client;
using CleanFramework.Service.Infrastructure.Filters;
using Confluent.Kafka;
using FluentValidation.AspNetCore;
using Hangfire;
using IdentityServer4.AccessTokenValidation;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using Polly.Registry;
using System;

namespace CleanFramework.Service.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCaching(this IServiceCollection services)
        {
            return services.AddMemoryCache()
                .AddEasyCaching(options =>
                {
                    options.UseInMemory();
                })
                .AddSingleton<IAppCache, AppCache>();
        }

        public static IServiceCollection AddApplicationLogging(this IServiceCollection services)
        {
            return services;
        }

        public static IServiceCollection AddApplicationHangfire(this IServiceCollection services)
        {
            const int DefaultRetryAttempts = 5;
            const int DefaultSecondsFactor = 2;

            services.AddHangfire(cfg =>
            {
                cfg.UseSimpleAssemblyNameTypeSerializer()
                    .UseRecommendedSerializerSettings()
                    // [Important] retry global config => Jobs must be Idempotence
                    .UseFilter(new AutomaticRetryAttribute()
                    {
                        Attempts = DefaultRetryAttempts,
                        DelayInSecondsByAttemptFunc = (attempt) => (int)(DefaultSecondsFactor * attempt)
                    })
                    .UseInMemoryStorage();
            });

            services.AddHangfireServer(opt =>
            {
                // [Important] specify a server name or leave it randomly
                opt.ServerName = Startup.HangfireSettings.ServerName;
            });

            return services;
        }

        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            string connStr = configuration.GetConnectionString(nameof(OrderingContext));

            if (connStr is null) throw new ArgumentNullException(nameof(connStr));

            services.AddDbContext<OrderingContext>(options => options
                .UseSqlServer(connStr)
                .UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll));

            return services;
        }

        public static IServiceCollection AddMediator(this IServiceCollection services)
        {
            services.Scan(scan => scan.FromApplicationDependencies(
                    assembly => assembly.GetName()?.Name?.StartsWith(nameof(CleanFramework)) == true)
                .AddClasses(classes => classes.AssignableTo(typeof(IPipelineBehavior<,>)))
                .As(typeof(IPipelineBehavior<,>)));

            return services.AddMediatR(typeof(Startup),
                typeof(Application.AssemblyModel));
        }

        public static IServiceCollection AddResilience(this IServiceCollection services)
        {
            return services.AddSingleton<IReadOnlyPolicyRegistry<string>, PolicyRegistry>(
                (serviceProvider) => ResiliencePolicy.InitPolly(serviceProvider));
        }

        public static IServiceCollection AddApplicationAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                // JWT tokens
                .AddJwtBearer(IdentityServerAuthenticationDefaults.AuthenticationScheme, opt =>
                {
                    opt.Authority = Startup.AppSettings.IdentityProviderUrl;
                    opt.Audience = Startup.AppSettings.ApplicationClientId;
                })
                .AddScheme<ClientAuthenticationOptions, ClientAuthenticationHandler>(
                    ClientAuthenticationDefaults.AuthenticationScheme, opt =>
                    {
                        opt.Clients = Startup.ClientSettings?.Clients;
                    });

            return services;
        }

        public static IServiceCollection AddApplicationAuthorization(this IServiceCollection services)
        {
            return services.AddAuthorization(opt =>
            {
                opt.AddPolicy(ApplicationPolicy.IsSpecialUser, builder =>
                    builder.AddRequirements(new IsSpecialUserRequirement()));

            }).AddSingleton<IAuthorizationPolicyProvider, ApplicationPolicyProvider>();
        }

        public static IServiceCollection AddPubSubIntegration(this IServiceCollection services, IConfiguration configuration)
        {
            var isEnabled = configuration.GetValue<bool>(ConfigurationKeys.ApacheKafka.Enabled);

            if (!isEnabled)
            {
                return services.AddNullPubSub();
            }

            return services
                .Configure<ProducerConfig>(
                    configuration.GetSection(ConfigurationKeys.ApacheKafka.ProducerConfig))
                .Configure<ConsumerConfig>(
                    configuration.GetSection(ConfigurationKeys.ApacheKafka.ConsumerConfig))
                .AddKafkaIntegration();
        }

        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            return services.AddAutoMapper(typeof(Startup),
                typeof(Application.AssemblyModel));
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGenNewtonsoftSupport();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "My API",
                    Description = "A simple example ASP.NET Core Web API",
                    TermsOfService = new Uri("https://example.com/terms"),
                });

                c.CustomSchemaIds(type => type.FullName);

                c.EnableAnnotations();

                const string ApplicationApiKey = nameof(ApplicationApiKey);
                const string ApplicationClient = nameof(ApplicationClient);

                c.AddSecurityDefinition(ApplicationApiKey,
                    new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Description = "Please enter an Authorization Header value",
                        Name = HeaderNames.Authorization,
                        Type = SecuritySchemeType.ApiKey,
                    });

                c.AddSecurityDefinition(ApplicationClient,
                    new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Description = "Please enter a credentials",
                        Name = HeaderNames.Authorization,
                        Type = SecuritySchemeType.Http,
                        Scheme = ClientAuthenticationDefaults.AuthenticationScheme
                    });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = ApplicationApiKey
                            }
                        },
                        new string[0]
                    },
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = ApplicationClient
                            }
                        },
                        new string[0]
                    }
                });
            });

            return services;
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            return services.ScanServices(
                assembly => assembly.GetName()?.Name?.StartsWith(nameof(CleanFramework)) == true);
        }

        public static IServiceCollection ConfigureApiBehavior(this IServiceCollection services)
        {
            return services.Configure<ApiBehaviorOptions>(opt =>
            {
                // [Important] Disable automatic 400 response
                opt.SuppressModelStateInvalidFilter = true;
            });
        }

        public static IMvcBuilder AddApplicationControllers(this IServiceCollection services)
        {
            return services.AddControllers(opt =>
            {
                opt.Filters.Add<ValidateModelStateFilter>();
                opt.Filters.Add<ApiExceptionFilter>();
                opt.Filters.Add<ApiResponseWrapperFilter>();
            });
        }

        public static IMvcBuilder AddValidation(this IMvcBuilder mvcBuilder)
        {
            return mvcBuilder.AddFluentValidation(opt =>
            {
                opt.RegisterValidatorsFromAssemblies(new[]
                {
                    typeof(Startup).Assembly,
                    typeof(Application.AssemblyModel).Assembly
                }, lifetime: ServiceLifetime.Scoped);
                opt.DisableDataAnnotationsValidation = true;
                opt.ImplicitlyValidateChildProperties = true;
                opt.ImplicitlyValidateRootCollectionElements = true;
                opt.AutomaticValidationEnabled = true;
                opt.ValidatorOptions.DisplayNameResolver = FluentValidationDisplayNameResolver.Resolve;
            });
        }
    }
}
