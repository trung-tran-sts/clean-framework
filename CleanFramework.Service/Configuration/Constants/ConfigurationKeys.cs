﻿namespace CleanFramework.Service.Configuration.Constants
{
    public static class ConfigurationKeys
    {
        public static class ApacheKafka
        {
            public const string ApacheKafkaSettings = nameof(ApacheKafkaSettings);
            public const string Enabled = ApacheKafkaSettings + ":" + nameof(Enabled);
            public const string ProducerConfig = ApacheKafkaSettings + ":" + nameof(ProducerConfig);
            public const string ConsumerConfig = ApacheKafkaSettings + ":" + nameof(ConsumerConfig);
        }
    }
}
