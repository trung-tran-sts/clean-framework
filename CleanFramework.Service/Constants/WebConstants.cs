﻿namespace CleanFramework.Service.Constants
{
    public static class EnvironmentVariables
    {
        public static class AspNetCoreEnv
        {
            public const string Key = "ASPNETCORE_ENVIRONMENT";
            public const string Development = nameof(Development);
        }
    }
}
